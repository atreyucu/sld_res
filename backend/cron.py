from dateutil.relativedelta import relativedelta
import cronjobs
import datetime
from backend.models import SmkLqdCohort, SmkLqdSubsStandingOrder, SmkLqdOrderOrder, SmkLqdOrderBatch, SmkLqdOrderItem, ProductProduct, SmkLqdSnailLetter, SmkLqdSnailBatch, SmkLqdSnailMail, SmkLqdSubsDailyRefund, SmkLqdNotificationEmail, SmkLqdPrizeRegistration, SmkLqdRecurringPaymentSettings
from django.db import connection, transaction
from common.date_tools import get_next_billing
from sld_site import settings
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEImage import MIMEImage
import smtplib


@cronjobs.register
def cohort():
    SmkLqdCohort.objects.all().delete()
    from sld_site.settings import COHORT_START_DATE as cohort_start_date
    cohort_end_date = datetime.date.today()
    diff = cohort_end_date - cohort_start_date
    month_amount = diff.days/31

    return_data = []

    for cohort_index in range(1,month_amount+2):

        init_cant_subscribers = SmkLqdSubsStandingOrder.objects.filter(cohort_index=cohort_index).count()

        b_subscribers_cant = init_cant_subscribers

        cohort_data = {'Cohort ' + str(cohort_index): {}}
        month_data_str_arr = []

        sql_str_fields = ''
        sql_str_values = ''

        for month_index in range(0,60):

            month_subscribers = b_subscribers_cant

            months_sum = cohort_index -1 + month_index
            month_start_date = cohort_start_date +  relativedelta(months=months_sum)
            month_end_date = cohort_start_date +  relativedelta(months=months_sum + 1)

            current_month = datetime.date.today().month
            current_year = datetime.date.today().year
            next_month_date = datetime.date(current_year, current_month + 1, 1)

            month_cancelled = SmkLqdSubsStandingOrder.objects.filter(cohort_index=cohort_index).filter(cancelled_on__range = (month_start_date, month_end_date)).filter(status = 'n').count()

            month_still_active = month_subscribers - month_cancelled
            if month_subscribers != 0:
                month_percent = float(month_cancelled)/float(month_subscribers)*100
            else:
                month_percent = 0
            b_subscribers_cant = month_still_active

            month_data = {'subscribers': month_subscribers, 'still_active': month_still_active, 'percent': format(month_percent, '.2f')}

            #salvando string del mes para insertar en la bd

            month_data_str = '%s/%s(%s%%%%)'%(str(month_subscribers),str(month_still_active), str(format(month_percent, '.2f')))
            #--------------salvando string del mes para insertar en la bd---------------------------

            cohort_data['Cohort ' + str(cohort_index)]['month' + str(month_index)] = month_data

            if month_start_date < next_month_date:
                cohort_data['Cohort ' + str(cohort_index)]['month' + str(month_index)]['text'] = str(month_subscribers) + '/' + str(month_still_active) + '(' + format(month_percent, '.2f') +'%)'
            else:
                cohort_data['Cohort ' + str(cohort_index)]['month' + str(month_index)]['text'] = '/'
                month_data_str = '/'


            sql_str_fields += " month" + str(month_index + 1) + ","
            sql_str_values +=  " ' " + month_data_str + " ', "

        if init_cant_subscribers != 0:
            total_canceled = init_cant_subscribers - month_still_active
            total_percent = float(total_canceled)/float(init_cant_subscribers)*100
        else:
            total_percent = 0
        total_data = {'subscribers': init_cant_subscribers, 'still_active': month_still_active, 'percent': format(total_percent, '.2f')}

        total_data_str = "'" + '%s/%s(%s%%%%)'%(str(init_cant_subscribers), str(month_still_active), str(format(total_percent, '.2f'))) + "'"

        cohort_data['Cohort ' + str(cohort_index)]['total'] = total_data


        cohort_data['Cohort ' + str(cohort_index)]['index'] = cohort_index

        sql_str_fields += "total, cohort_index"
        sql_str_values += total_data_str + " , 'Cohort " + str(cohort_index) +"'"


        insert_sql = 'INSERT INTO  smk_lqd_cohort(%s) VALUES (%s)'%(sql_str_fields, sql_str_values)

        #SmkLqdCohort.objects.raw(insert_sql)
        cursor = connection.cursor()
        cursor.execute(insert_sql)
        transaction.commit_unless_managed()

        #print insert_sql
        return_data.append(cohort_data)


@cronjobs.register
def renewal():
    for soi in SmkLqdSubsStandingOrder.objects.filter(status='y',fraud='n'):
        #print soi.customer_name
        if (datetime.date.today() - soi.next_printed).days == 0:
            order = SmkLqdOrderOrder()
            order.create_date = datetime.datetime.now()
            order.created_on = datetime.date.today()
            order.customer = soi.customer
            billing_addr = soi.customer.respartneraddress_set.filter(type='billing')[0]
            delivery_addr = soi.customer.respartneraddress_set.filter(type='delivery')[0]
            order.customer_billing_hbn = billing_addr.hbn
            order.customer_billing_street = billing_addr.street
            order.customer_billing_street_line2 = billing_addr.street2
            order.customer_billing_country = billing_addr.country.name
            order.customer_billing_county = billing_addr.state.name
            order.customer_billing_city = billing_addr.city
            order.customer_billing_postcode = billing_addr.zip

            order.customer_delivery_hbn = delivery_addr.hbn
            order.customer_delivery_street = delivery_addr.street
            order.customer_delivery_street_line2 = delivery_addr.street2
            order.customer_delivery_country = delivery_addr.country.name
            order.customer_delivery_county = delivery_addr.state.name
            order.customer_delivery_city = delivery_addr.city
            order.customer_delivery_postcode = delivery_addr.zip

            order.require_envelope = 'n'
            order.printed = 'n'
            order.manual = 'n'
            order.customer_name = soi.customer.name
            order.order_total = 19.99
            import random
            import time
            order.trans_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
            order.customer_email = soi.customer.username
            order.customer_fax = delivery_addr.fax
            order.customer_mobile = delivery_addr.mobile
            order.customer_phone = delivery_addr.phone

            if SmkLqdOrderBatch.objects.filter(require_envelope='n').count() > 0:
                    if SmkLqdOrderBatch.objects.filter(require_envelope='n').order_by('-id')[0].smklqdorderorder_set.count() <= settings.DEFAULT_ORDER_NUMBER_PER_BATCH and not SmkLqdOrderBatch.objects.filter(require_envelope='n').order_by('-id')[0].printed == 'y':
                        batch = SmkLqdOrderBatch.objects.filter(require_envelope='n').order_by('-id')[0]
                        batch.order_count += 1
                        batch.save()
                    else:
                        batch = SmkLqdOrderBatch()
                        batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
                        batch.created_on = datetime.date.today()
                        batch.potential_fraud = 'n'
                        batch.require_envelope = 'n'
                        batch.manual = 'n'
                        batch.order_count = 1
                        batch.envelope_printed = 'n'
                        batch.printed = 'n'
                        batch.save()
            else:
                batch = SmkLqdOrderBatch()
                batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
                batch.created_on = datetime.date.today()
                batch.potential_fraud = 'n'
                batch.require_envelope = 'n'
                batch.manual = 'n'
                batch.order_count = 1
                batch.envelope_printed = 'n'
                batch.printed = 'n'
                batch.save()


            order.batch_ids = batch
            order.save()

            #item para el flavour 1
            temp_product = soi.flavours.flavour1
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 2
            temp_product = soi.flavours.flavour2
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 3
            temp_product = soi.flavours.flavour3
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 4
            temp_product = soi.flavours.flavour4
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 5
            temp_product = soi.flavours.flavour5
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 6
            temp_product = soi.flavours.flavour6
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 7
            temp_product = soi.flavours.flavour7
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 8
            temp_product = soi.flavours.flavour8
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 9
            temp_product = soi.flavours.flavour9
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 10
            temp_product = soi.flavours.flavour10
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el free flavour 1
            temp_product = soi.free_flavours.flavour1
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 1

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el free flavour 2
            temp_product = soi.free_flavours.flavour2
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 1

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el free flavour 3
            temp_product = soi.free_flavours.flavour3
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 1

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el free flavour 4
            temp_product = soi.free_flavours.flavour4
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 1

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            if soi.free_vaporizer_count > 0:
                temp_product = ProductProduct.objects.get(default_code='F-1')
                item = SmkLqdOrderItem()
                item.order_ids = order
                item.product = temp_product
                item.line_total = 2 + soi.free_vaporizer_count

                item.product_name = temp_product.name_template
                item.product_description = temp_product.product_tmpl.description
                item.save()
            else:
                temp_product = ProductProduct.objects.get(default_code='F-1')
                item = SmkLqdOrderItem()
                item.order_ids = order
                item.product = temp_product
                item.line_total = 2

                item.product_name = temp_product.name_template
                item.product_description = temp_product.product_tmpl.description
                item.save()


            if soi.order_counter%3 == 0:
                temp_product = ProductProduct.objects.get(default_code='E-1')
                item = SmkLqdOrderItem()
                item.order_ids = order
                item.product = temp_product
                item.line_total = 1

                item.product_name = temp_product.name_template
                item.product_description = temp_product.product_tmpl.description
                item.save()


            soi.last_batchid = batch.batch_id
            soi.order_counter += 1
            soi.save()

            if SmkLqdRecurringPaymentSettings.objects.all().count() > 0:
                recurring_settings = SmkLqdRecurringPaymentSettings.objects.all()[0]

                days_count = recurring_settings.days_per_period
                minus_days = recurring_settings.printing_minus_days
            else:
                days_count = 61
                minus_days = 10

            soi.last_order_date = datetime.date.today()
            soi.free_vaporizer_count = 0
            soi.last_billing = datetime.date.today()
            soi.last_printed = datetime.date.today()
            soi.next_printed = datetime.date.today() + datetime.timedelta(days = days_count - minus_days)
            soi.next_billing = get_next_billing(datetime.date.today())
            soi.save()

@cronjobs.register
def delivery_date_updater():
    for soi in SmkLqdSubsStandingOrder.objects.filter(status='y',fraud='n'):
        if SmkLqdRecurringPaymentSettings.objects.all().count() > 0:
            recurring_settings = SmkLqdRecurringPaymentSettings.objects.all()[0]

            days_count = recurring_settings.days_per_period
            minus_days = recurring_settings.printing_minus_days
        else:
            days_count = 61
            minus_days = 10

        if (datetime.date.today() - soi.next_delivery).days == 0:
            temp_value = soi.next_delivery
            temp_value2 = soi.last_delivery
            soi.last_delivery = temp_value
            soi.next_delivery = temp_value2 + datetime.timedelta(days = days_count)
            soi.save()


@cronjobs.register
def snail_mail():
    for letter in SmkLqdSnailLetter.objects.filter(status='y'):
        for soi in SmkLqdSubsStandingOrder.objects.filter(status='y',fraud='n'):
            allow_exec = False

            if letter.frequency == 'OAO' and (soi.next_printed - datetime.date.today()).days == letter.days_before_order:
                allow_exec = True

            elif letter.frequency == 'ESO' and (soi.next_printed - datetime.date.today()).days == letter.days_before_order and soi.snail_mail_counter == letter.specific_order:
                allow_exec = True

            elif (soi.next_printed - datetime.date.today()).days == letter.days_before_order:
                every_order = int(letter.frequency[2])
                beginning_in = letter.beginning_in


                if (soi.snail_mail_counter - letter.beginning_in)%every_order == 0:
                    allow_exec = True


            import random, time

            if allow_exec:
                if SmkLqdSnailBatch.objects.count() > 0:
                    if SmkLqdSnailBatch.objects.filter(status = 'n').count() > 0 and SmkLqdSnailBatch.objects.filter(status = 'n').order_by('-id')[0].smklqdsnailmail_set.count() <= settings.DEFAULT_LETTER_NUMBER_PER_BATCH:
                        snail_batch = SmkLqdSnailBatch.objects.filter(status = 'n').order_by('-id')[0]
                    else:
                        snail_batch = SmkLqdSnailBatch()
                        snail_batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
                        snail_batch.generated_date = datetime.date.today()
                        snail_batch.status = 'n'
                        snail_batch.email_count = 0
                        snail_batch.save()
                else:
                    snail_batch = SmkLqdSnailBatch()
                    snail_batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
                    snail_batch.generated_date = datetime.date.today()
                    snail_batch.status = 'n'
                    snail_batch.email_count = 0
                    snail_batch.save()




                snail_letter = SmkLqdSnailMail()
                snail_letter.email_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
                snail_letter.customer = soi.customer
                snail_letter.status = 'n'
                snail_letter.customer_active = 'y'
                snail_letter.customer_name = soi.customer_name
                snail_letter.batch_ids = snail_batch
                snail_letter.letter = letter
                snail_letter.save()

                snail_batch.email_count += 1
                snail_batch.save()




                soi.snail_mail_counter += 1
                soi.save()

@cronjobs.register
def refund():
    created = False
    for soi in SmkLqdSubsStandingOrder.objects.filter(status='y'):
        if (datetime.date.today() - soi.created_date).days == settings.REFUND_PERIOD:
            if not created:
                batch = SmkLqdSubsDailyRefund()
                batch.status = 'p'
                batch.refunds_due += 1
                batch.refund_date = datetime.date.today()
                batch.signup_date = soi.created_date

                batch.save()
                created = True

            soi.daily_refund_ids = batch
            soi.save()

@cronjobs.register
def email_notification():
    for email in SmkLqdNotificationEmail.objects.filter(status='y', hour=str(datetime.datetime.now().hour), minute=str(datetime.datetime.now().minute)).exclude(email_list='c'):
        #print email.email_subject
        #email = SmkLqdNotificationEmail()#OJO
        subject_value = email.email_subject
        email_content = email.email_content
        from jinja2 import Template

        snail_template = Template(email.email_template.template_content)

        html = snail_template.render(subject=subject_value,
                                    content=email_content)

        if(email.email_from_addr):
            from_email_addr = email.email_from_addr.email_addr
        else:
            from_email_addr = settings.EMAIL_HOST_USER

        if(email.email_list == 'a' and email.email_condition_lista == 'ca2'):
            for reg in SmkLqdPrizeRegistration.objects.all():
                #reg = SmkLqdPrizeRegistration()
                if reg.signup_date and (datetime.date.today() - reg.signup_date).days == email.email_condition_value:
                    #print 'sending email'
                    msgRoot = MIMEMultipart('related')
                    msgRoot['Subject'] = subject_value
                    msgRoot['From'] = from_email_addr
                    msgRoot['To'] = reg.email
                    msgRoot.preamble = subject_value

                    msgAlternative = MIMEMultipart('alternative')
                    msgRoot.attach(msgAlternative)

                    msgText = MIMEText(html, 'html')
                    msgAlternative.attach(msgText)

                    fp = open(settings.EMAIL_IMAGE, 'rb')
                    msgImage = MIMEImage(fp.read())
                    fp.close()

                    msgImage.add_header('Content-ID', '<image1>')
                    msgRoot.attach(msgImage)
                    if settings.USE_SSL:
                        try:
                            smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                            if email.email_server.smtp_user and email.email_server.smtp_pass:
                                smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                            smt.sendmail(from_email_addr,[reg.email],msgRoot.as_string())
                            smt.close()
                        except Exception,ex:
                            print ex
                            pass
                    else:
                        try:
                            #send_mail(subject_value, message_value, settings.AES_FROM_USER,to_value)
                            smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                            if email.email_server.smtp_user:
                                smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                            smt.sendmail(from_email_addr,[reg.email],msgRoot.as_string())
                            smt.close()
                        except Exception,ex:
                            print ex
                            pass

        elif(email.email_list == 'b'):
            if(email.email_condition_listb == 'cb1'):
                for subs in SmkLqdSubsStandingOrder.objects.filter(status='y'):
                    if subs.reactivated == 'y':
                        if (datetime.date.today() - subs.reactivated_date).days > 3 and (datetime.date.today() - subs.reactivated_date).days == email.email_condition_value:
                            msgRoot = MIMEMultipart('related')
                            msgRoot['Subject'] = subject_value
                            msgRoot['From'] = from_email_addr
                            msgRoot['To'] = subs.customer_email
                            msgRoot.preamble = subject_value

                            msgAlternative = MIMEMultipart('alternative')
                            msgRoot.attach(msgAlternative)

                            msgText = MIMEText(html, 'html')
                            msgAlternative.attach(msgText)

                            fp = open(settings.EMAIL_IMAGE, 'rb')
                            msgImage = MIMEImage(fp.read())
                            fp.close()

                            msgImage.add_header('Content-ID', '<image1>')
                            msgRoot.attach(msgImage)
                            if settings.USE_SSL:
                                try:
                                    smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass
                            else:
                                try:
                                    #send_mail(subject_value, message_value, settings.AES_FROM_USER,to_value)
                                    smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass
                    else:
                        if (datetime.date.today() - subs.created_date).days == email.email_condition_value:
                            msgRoot = MIMEMultipart('related')
                            msgRoot['Subject'] = subject_value
                            msgRoot['From'] = from_email_addr
                            msgRoot['To'] = subs.customer_email
                            msgRoot.preamble = subject_value

                            msgAlternative = MIMEMultipart('alternative')
                            msgRoot.attach(msgAlternative)

                            msgText = MIMEText(html, 'html')
                            msgAlternative.attach(msgText)

                            fp = open(settings.EMAIL_IMAGE, 'rb')
                            msgImage = MIMEImage(fp.read())
                            fp.close()

                            msgImage.add_header('Content-ID', '<image1>')
                            msgRoot.attach(msgImage)
                            if settings.USE_SSL:
                                try:
                                    smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass
                            else:
                                try:
                                    #send_mail(subject_value, message_value, settings.AES_FROM_USER,to_value)
                                    smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass

            elif(email.email_condition_listb == 'cb2'):
                if(email.frequency == '1'):
                    for subs in SmkLqdSubsStandingOrder.objects.filter(status='y'):
                        allow_exec = False

                        if (subs.order_counter - (email.beginning_in + 1)) >= 0:
                            allow_exec = True

                        if (datetime.date.today() - subs.last_printed).days == email.email_condition_value and allow_exec:
                            msgRoot = MIMEMultipart('related')
                            msgRoot['Subject'] = subject_value
                            msgRoot['From'] = from_email_addr
                            msgRoot['To'] = subs.customer_email
                            msgRoot.preamble = subject_value

                            msgAlternative = MIMEMultipart('alternative')
                            msgRoot.attach(msgAlternative)

                            msgText = MIMEText(html, 'html')
                            msgAlternative.attach(msgText)

                            fp = open(settings.EMAIL_IMAGE, 'rb')
                            msgImage = MIMEImage(fp.read())
                            fp.close()

                            msgImage.add_header('Content-ID', '<image1>')
                            msgRoot.attach(msgImage)
                            if settings.USE_SSL:
                                try:
                                    smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass
                            else:
                                try:
                                    #send_mail(subject_value, message_value, settings.AES_FROM_USER,to_value)
                                    smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass

                else:
                    for subs in SmkLqdSubsStandingOrder.objects.filter(status='y'):
                        every_order = int(email.frequency)

                        if (subs.order_counter - email.beginning_in + 1)%every_order == 0:
                            allow_exec = True

                        if (datetime.date.today() - subs.last_printed).days == email.email_condition_value and  allow_exec:
                            msgRoot = MIMEMultipart('related')
                            msgRoot['Subject'] = subject_value
                            msgRoot['From'] = from_email_addr
                            msgRoot['To'] = subs.customer_email
                            msgRoot.preamble = subject_value

                            msgAlternative = MIMEMultipart('alternative')
                            msgRoot.attach(msgAlternative)

                            msgText = MIMEText(html, 'html')
                            msgAlternative.attach(msgText)

                            fp = open(settings.EMAIL_IMAGE, 'rb')
                            msgImage = MIMEImage(fp.read())
                            fp.close()

                            msgImage.add_header('Content-ID', '<image1>')
                            msgRoot.attach(msgImage)
                            if settings.USE_SSL:
                                try:
                                    smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass
                            else:
                                try:
                                    #send_mail(subject_value, message_value, settings.AES_FROM_USER,to_value)
                                    smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass

            elif(email.email_condition_listb == 'cb3'):
                if(email.frequency == '1'):
                    for subs in SmkLqdSubsStandingOrder.objects.filter(status='y'):
                        allow_exec = False

                        if (subs.order_counter - email.beginning_in) >= 0:
                            allow_exec = True

                        if (subs.next_delivery - datetime.date.today()).days == email.email_condition_value and allow_exec:
                            msgRoot = MIMEMultipart('related')
                            msgRoot['Subject'] = subject_value
                            msgRoot['From'] = from_email_addr
                            msgRoot['To'] = subs.customer_email
                            msgRoot.preamble = subject_value

                            msgAlternative = MIMEMultipart('alternative')
                            msgRoot.attach(msgAlternative)

                            msgText = MIMEText(html, 'html')
                            msgAlternative.attach(msgText)

                            fp = open(settings.EMAIL_IMAGE, 'rb')
                            msgImage = MIMEImage(fp.read())
                            fp.close()

                            msgImage.add_header('Content-ID', '<image1>')
                            msgRoot.attach(msgImage)
                            if settings.USE_SSL:
                                try:
                                    smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass
                            else:
                                try:
                                    #send_mail(subject_value, message_value, settings.AES_FROM_USER,to_value)
                                    smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass
                else:
                    for subs in SmkLqdSubsStandingOrder.objects.filter(status='y'):
                        every_order = int(email.frequency)

                        if (subs.order_counter - email.beginning_in)%every_order == 0:
                            allow_exec = True

                        if (subs.next_delivery - datetime.date.today()).days == email.email_condition_value and  allow_exec:
                            msgRoot = MIMEMultipart('related')
                            msgRoot['Subject'] = subject_value
                            msgRoot['From'] = from_email_addr
                            msgRoot['To'] = subs.customer_email
                            msgRoot.preamble = subject_value

                            msgAlternative = MIMEMultipart('alternative')
                            msgRoot.attach(msgAlternative)

                            msgText = MIMEText(html, 'html')
                            msgAlternative.attach(msgText)

                            fp = open(settings.EMAIL_IMAGE, 'rb')
                            msgImage = MIMEImage(fp.read())
                            fp.close()

                            msgImage.add_header('Content-ID', '<image1>')
                            msgRoot.attach(msgImage)
                            if settings.USE_SSL:
                                try:
                                    smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass
                            else:
                                try:
                                    #send_mail(subject_value, message_value, settings.AES_FROM_USER,to_value)
                                    smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass



            elif(email.email_condition_listb == 'cb4'):
                for subs in SmkLqdSubsStandingOrder.objects.filter(status='y'):
                    if (datetime.date.today() - subs.last_printed).days == email.email_condition_value and subs.order_counter - 1 == email.order_number:
                        msgRoot = MIMEMultipart('related')
                        msgRoot['Subject'] = subject_value
                        msgRoot['From'] = from_email_addr
                        msgRoot['To'] = subs.customer_email
                        msgRoot.preamble = subject_value

                        msgAlternative = MIMEMultipart('alternative')
                        msgRoot.attach(msgAlternative)

                        msgText = MIMEText(html, 'html')
                        msgAlternative.attach(msgText)

                        fp = open(settings.EMAIL_IMAGE, 'rb')
                        msgImage = MIMEImage(fp.read())
                        fp.close()

                        msgImage.add_header('Content-ID', '<image1>')
                        msgRoot.attach(msgImage)
                        if settings.USE_SSL:
                            try:
                                smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                if email.email_server.smtp_user:
                                    smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                smt.close()
                            except Exception,ex:
                                print ex
                                pass
                        else:
                            try:
                                #send_mail(subject_value, message_value, settings.AES_FROM_USER,to_value)
                                smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                if email.email_server.smtp_user:
                                    smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                smt.close()
                            except Exception,ex:
                                print ex
                                pass

            elif(email.email_condition_listb == 'cb5'):
                for subs in SmkLqdSubsStandingOrder.objects.filter(status='y'):
                    if (subs.next_delivery - datetime.date.today()).days == email.email_condition_value and subs.order_counter == email.order_number:
                        msgRoot = MIMEMultipart('related')
                        msgRoot['Subject'] = subject_value
                        msgRoot['From'] = from_email_addr
                        msgRoot['To'] = subs.customer_email
                        msgRoot.preamble = subject_value

                        msgAlternative = MIMEMultipart('alternative')
                        msgRoot.attach(msgAlternative)

                        msgText = MIMEText(html, 'html')
                        msgAlternative.attach(msgText)

                        fp = open(settings.EMAIL_IMAGE, 'rb')
                        msgImage = MIMEImage(fp.read())
                        fp.close()

                        msgImage.add_header('Content-ID', '<image1>')
                        msgRoot.attach(msgImage)
                        if settings.USE_SSL:
                            try:
                                smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                if email.email_server.smtp_user:
                                    smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                smt.close()
                            except Exception,ex:
                                print ex
                                pass
                        else:
                            try:
                                #send_mail(subject_value, message_value, settings.AES_FROM_USER,to_value)
                                smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                if email.email_server.smtp_user:
                                    smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                smt.close()
                            except Exception,ex:
                                print ex
                                pass





@cronjobs.register
def email_notification2():
    for email in SmkLqdNotificationEmail.objects.filter(status='y').exclude(email_list='c'):
        #email = SmkLqdNotificationEmail()#OJO
        subject_value = email.email_subject
        email_content = email.email_content
        from jinja2 import Template

        snail_template = Template(email.email_template.template_content)

        html = snail_template.render(subject=subject_value,
                                    content=email_content)

        if(email.email_from_addr):
            from_email_addr = email.email_from_addr.email_addr
        else:
            from_email_addr = settings.EMAIL_HOST_USER

        if(email.email_list == 'a' and email.email_condition_lista == 'ca2'):
            for reg in SmkLqdPrizeRegistration.objects.all():
                #reg = SmkLqdPrizeRegistration()
                if reg.signup_date and (datetime.date.today() - reg.signup_date).days == email.email_condition_value:
                    print subject_value
                    msgRoot = MIMEMultipart('related')
                    msgRoot['Subject'] = subject_value
                    msgRoot['From'] = from_email_addr
                    msgRoot['To'] = reg.email
                    msgRoot.preamble = subject_value

                    msgAlternative = MIMEMultipart('alternative')
                    msgRoot.attach(msgAlternative)

                    msgText = MIMEText(html, 'html')
                    msgAlternative.attach(msgText)

                    fp = open(settings.EMAIL_IMAGE, 'rb')
                    msgImage = MIMEImage(fp.read())
                    fp.close()

                    msgImage.add_header('Content-ID', '<image1>')
                    msgRoot.attach(msgImage)
                    if settings.USE_SSL:
                        try:
                            smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                            if email.email_server.smtp_user and email.email_server.smtp_pass:
                                smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                            smt.sendmail(from_email_addr,[reg.email],msgRoot.as_string())
                            smt.close()
                        except Exception,ex:
                            print ex
                            pass
                    else:
                        try:
                            #send_mail(subject_value, message_value, settings.AES_FROM_USER,to_value)
                            smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                            if email.email_server.smtp_user:
                                smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                            smt.sendmail(from_email_addr,[reg.email],msgRoot.as_string())
                            smt.close()
                        except Exception,ex:
                            print ex
                            pass

        elif(email.email_list == 'b'):
            if(email.email_condition_listb == 'cb1'):
                for subs in SmkLqdSubsStandingOrder.objects.filter(status='y'):
                    if (datetime.date.today() - subs.created_date).days == email.email_condition_value:
                        print subject_value
                        msgRoot = MIMEMultipart('related')
                        msgRoot['Subject'] = subject_value
                        msgRoot['From'] = from_email_addr
                        msgRoot['To'] = subs.customer_email
                        msgRoot.preamble = subject_value

                        msgAlternative = MIMEMultipart('alternative')
                        msgRoot.attach(msgAlternative)

                        msgText = MIMEText(html, 'html')
                        msgAlternative.attach(msgText)

                        fp = open(settings.EMAIL_IMAGE, 'rb')
                        msgImage = MIMEImage(fp.read())
                        fp.close()

                        msgImage.add_header('Content-ID', '<image1>')
                        msgRoot.attach(msgImage)
                        if settings.USE_SSL:
                            try:
                                smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                if email.email_server.smtp_user:
                                    smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                smt.close()
                            except Exception,ex:
                                print ex
                                pass
                        else:
                            try:
                                #send_mail(subject_value, message_value, settings.AES_FROM_USER,to_value)
                                smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                if email.email_server.smtp_user:
                                    smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                smt.close()
                            except Exception,ex:
                                print ex
                                pass

            elif(email.email_condition_listb == 'cb2'):
                if(email.frequency == '1'):
                    for subs in SmkLqdSubsStandingOrder.objects.filter(status='y'):
                        allow_exec = False

                        if (subs.order_counter - (email.beginning_in + 1)) >= 0:
                            allow_exec = True

                        if (datetime.date.today() - subs.last_printed).days == email.email_condition_value and allow_exec:
                            print subject_value
                            msgRoot = MIMEMultipart('related')
                            msgRoot['Subject'] = subject_value
                            msgRoot['From'] = from_email_addr
                            msgRoot['To'] = subs.customer_email
                            msgRoot.preamble = subject_value

                            msgAlternative = MIMEMultipart('alternative')
                            msgRoot.attach(msgAlternative)

                            msgText = MIMEText(html, 'html')
                            msgAlternative.attach(msgText)

                            fp = open(settings.EMAIL_IMAGE, 'rb')
                            msgImage = MIMEImage(fp.read())
                            fp.close()

                            msgImage.add_header('Content-ID', '<image1>')
                            msgRoot.attach(msgImage)
                            if settings.USE_SSL:
                                try:
                                    smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass
                            else:
                                try:
                                    #send_mail(subject_value, message_value, settings.AES_FROM_USER,to_value)
                                    smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass

                else:
                    for subs in SmkLqdSubsStandingOrder.objects.filter(status='y'):
                        every_order = int(email.frequency)

                        if (subs.order_counter - email.beginning_in + 1)%every_order == 0:
                            allow_exec = True

                        if (datetime.date.today() - subs.last_printed).days == email.email_condition_value and  allow_exec:
                            print subject_value
                            msgRoot = MIMEMultipart('related')
                            msgRoot['Subject'] = subject_value
                            msgRoot['From'] = from_email_addr
                            msgRoot['To'] = subs.customer_email
                            msgRoot.preamble = subject_value

                            msgAlternative = MIMEMultipart('alternative')
                            msgRoot.attach(msgAlternative)

                            msgText = MIMEText(html, 'html')
                            msgAlternative.attach(msgText)

                            fp = open(settings.EMAIL_IMAGE, 'rb')
                            msgImage = MIMEImage(fp.read())
                            fp.close()

                            msgImage.add_header('Content-ID', '<image1>')
                            msgRoot.attach(msgImage)
                            if settings.USE_SSL:
                                try:
                                    smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass
                            else:
                                try:
                                    #send_mail(subject_value, message_value, settings.AES_FROM_USER,to_value)
                                    smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass

            elif(email.email_condition_listb == 'cb3'):
                if(email.frequency == '1'):
                    for subs in SmkLqdSubsStandingOrder.objects.filter(status='y'):
                        allow_exec = False

                        if (subs.order_counter - email.beginning_in) >= 0:
                            allow_exec = True

                        if (subs.next_delivery - datetime.date.today()).days == email.email_condition_value and allow_exec:
                            print subject_value
                            msgRoot = MIMEMultipart('related')
                            msgRoot['Subject'] = subject_value
                            msgRoot['From'] = from_email_addr
                            msgRoot['To'] = subs.customer_email
                            msgRoot.preamble = subject_value

                            msgAlternative = MIMEMultipart('alternative')
                            msgRoot.attach(msgAlternative)

                            msgText = MIMEText(html, 'html')
                            msgAlternative.attach(msgText)

                            fp = open(settings.EMAIL_IMAGE, 'rb')
                            msgImage = MIMEImage(fp.read())
                            fp.close()

                            msgImage.add_header('Content-ID', '<image1>')
                            msgRoot.attach(msgImage)
                            if settings.USE_SSL:
                                try:
                                    smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass
                            else:
                                try:
                                    #send_mail(subject_value, message_value, settings.AES_FROM_USER,to_value)
                                    smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass
                else:
                    for subs in SmkLqdSubsStandingOrder.objects.filter(status='y'):
                        every_order = int(email.frequency)

                        if (subs.order_counter - email.beginning_in)%every_order == 0:
                            allow_exec = True

                        if (subs.next_delivery - datetime.date.today()).days == email.email_condition_value and  allow_exec:
                            print subject_value
                            msgRoot = MIMEMultipart('related')
                            msgRoot['Subject'] = subject_value
                            msgRoot['From'] = from_email_addr
                            msgRoot['To'] = subs.customer_email
                            msgRoot.preamble = subject_value

                            msgAlternative = MIMEMultipart('alternative')
                            msgRoot.attach(msgAlternative)

                            msgText = MIMEText(html, 'html')
                            msgAlternative.attach(msgText)

                            fp = open(settings.EMAIL_IMAGE, 'rb')
                            msgImage = MIMEImage(fp.read())
                            fp.close()

                            msgImage.add_header('Content-ID', '<image1>')
                            msgRoot.attach(msgImage)
                            if settings.USE_SSL:
                                try:
                                    smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass
                            else:
                                try:
                                    #send_mail(subject_value, message_value, settings.AES_FROM_USER,to_value)
                                    smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                    if email.email_server.smtp_user:
                                        smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                    smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                    smt.close()
                                except Exception,ex:
                                    print ex
                                    pass



            elif(email.email_condition_listb == 'cb4'):
                for subs in SmkLqdSubsStandingOrder.objects.filter(status='y'):
                    if (datetime.date.today() - subs.last_printed).days == email.email_condition_value and subs.order_counter - 1 == email.order_number:
                        print subject_value
                        msgRoot = MIMEMultipart('related')
                        msgRoot['Subject'] = subject_value
                        msgRoot['From'] = from_email_addr
                        msgRoot['To'] = subs.customer_email
                        msgRoot.preamble = subject_value

                        msgAlternative = MIMEMultipart('alternative')
                        msgRoot.attach(msgAlternative)

                        msgText = MIMEText(html, 'html')
                        msgAlternative.attach(msgText)

                        fp = open(settings.EMAIL_IMAGE, 'rb')
                        msgImage = MIMEImage(fp.read())
                        fp.close()

                        msgImage.add_header('Content-ID', '<image1>')
                        msgRoot.attach(msgImage)
                        if settings.USE_SSL:
                            try:
                                smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                if email.email_server.smtp_user:
                                    smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                smt.close()
                            except Exception,ex:
                                print ex
                                pass
                        else:
                            try:
                                #send_mail(subject_value, message_value, settings.AES_FROM_USER,to_value)
                                smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                if email.email_server.smtp_user:
                                    smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                smt.close()
                            except Exception,ex:
                                print ex
                                pass

            elif(email.email_condition_listb == 'cb5'):
                for subs in SmkLqdSubsStandingOrder.objects.filter(status='y'):
                    if (subs.next_delivery - datetime.date.today()).days == email.email_condition_value and subs.order_counter == email.order_number:
                        print subject_value
                        msgRoot = MIMEMultipart('related')
                        msgRoot['Subject'] = subject_value
                        msgRoot['From'] = from_email_addr
                        msgRoot['To'] = subs.customer_email
                        msgRoot.preamble = subject_value

                        msgAlternative = MIMEMultipart('alternative')
                        msgRoot.attach(msgAlternative)

                        msgText = MIMEText(html, 'html')
                        msgAlternative.attach(msgText)

                        fp = open(settings.EMAIL_IMAGE, 'rb')
                        msgImage = MIMEImage(fp.read())
                        fp.close()

                        msgImage.add_header('Content-ID', '<image1>')
                        msgRoot.attach(msgImage)
                        if settings.USE_SSL:
                            try:
                                smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                if email.email_server.smtp_user:
                                    smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                smt.close()
                            except Exception,ex:
                                print ex
                                pass
                        else:
                            try:
                                #send_mail(subject_value, message_value, settings.AES_FROM_USER,to_value)
                                smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
                                if email.email_server.smtp_user:
                                    smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
                                smt.sendmail(from_email_addr,[subs.customer_email],msgRoot.as_string())
                                smt.close()
                            except Exception,ex:
                                print ex
                                pass

@cronjobs.register
def update_titles():
    from backend.models import ResPartnerTitle

    temp_title = ResPartnerTitle()
    temp_title.domain = 'smoking'
    temp_title.name = 'Mrs.'
    temp_title.shortcut = 'Mrs.'
    temp_title.save()

    temp_title = ResPartnerTitle()
    temp_title.domain = 'smoking'
    temp_title.name = 'Mr.'
    temp_title.shortcut = 'Mr.'
    temp_title.save()

    temp_title = ResPartnerTitle()
    temp_title.domain = 'smoking'
    temp_title.name = 'Miss.'
    temp_title.shortcut = 'Miss.'
    temp_title.save()

    temp_title = ResPartnerTitle()
    temp_title.domain = 'smoking'
    temp_title.name = 'Ms.'
    temp_title.shortcut = 'Ms.'
    temp_title.save()

@cronjobs.register
def stats_collector():
    from backend.models import SmkLqdBoardOrder, SmkLqdBoardSubscriptions, SmkLqdBoardCancelledGraph1, SmkLqdBoardCancelledGraph2, SmkLqdInvites, SmkLqdSubsPriceGroup, SmkLqdBoardCancelledGraph3
    SmkLqdBoardOrder.objects.all().delete()

    pending_orders = SmkLqdOrderOrder.objects.filter(printed='n').count()
    tomorrow_orders = SmkLqdSubsStandingOrder.objects.filter(next_printed=datetime.date.today() + datetime.timedelta(days=1)).count()
    today_orders = SmkLqdSubsStandingOrder.objects.filter(last_printed=datetime.date.today()).count()

    board_order = SmkLqdBoardOrder()
    board_order.pending_orders = pending_orders
    board_order.todays_new_orders = today_orders
    board_order.tomorrow_orders = tomorrow_orders
    board_order.save()

    SmkLqdBoardSubscriptions.objects.all().delete()
    total_subs = SmkLqdSubsStandingOrder.objects.all().count()
    cancelled_subs = SmkLqdSubsStandingOrder.objects.filter(status='n').count()
    today_new_subs = SmkLqdSubsStandingOrder.objects.filter(created_date=datetime.date.today()).count()
    can_be_cancelled = 0

    board_subs = SmkLqdBoardSubscriptions()
    board_subs.can_be_cancelled_today = can_be_cancelled
    board_subs.total_subs = total_subs
    board_subs.todays_new_subs = today_new_subs
    board_subs.cancelled_subscriber = cancelled_subs
    board_subs.save()


    SmkLqdBoardCancelledGraph1.objects.all().delete()

    start_date = datetime.date.today() - datetime.timedelta(days=360)


    start_year = start_date.year
    start_month = start_date.month

    months_table = {1:'Jan',
                    2:'Feb',
                    3:'Mar',
                    4:'Apr',
                    5:'May',
                    6:'June',
                    7:'July',
                    8:'Aug',
                    9:'Sept',
                    10:'Oct',
                    11:'Nov',
                    12:'Dec'}

    year_count = start_year
    month_count = start_month

    iter_count = 1

    while(iter_count < 13):
        cancelled_count = SmkLqdSubsStandingOrder.objects.filter(status='n').filter(cancelled_on__month=month_count).count()
        new_subs_count = SmkLqdSubsStandingOrder.objects.filter(status='y').filter(created_date__month=month_count).count()


        temp_graph = SmkLqdBoardCancelledGraph1()
        temp_graph.month = '%s/%s'%(str(year_count),months_table[month_count])
        temp_graph.new_subs = new_subs_count
        temp_graph.cancelled_count = cancelled_count
        temp_graph.save()

        if(month_count == 12):
            month_count = 1
            year_count += 1
        else:
            month_count += 1

        iter_count+=1

    SmkLqdBoardCancelledGraph2.objects.all().delete()

    temp_graph2 = SmkLqdBoardCancelledGraph2()
    temp_graph2.signup_method = 'Through a referral'
    temp_graph2.count = SmkLqdInvites.objects.filter(convert_signup=True).count()
    temp_graph2.save()

    temp_graph2 = SmkLqdBoardCancelledGraph2()
    temp_graph2.signup_method = 'Signup visiting the site'
    temp_graph2.count = SmkLqdSubsStandingOrder.objects.all().count() - SmkLqdInvites.objects.filter(convert_signup=True).count()
    temp_graph2.save()

    SmkLqdBoardCancelledGraph3.objects.all().delete()

    for group in SmkLqdSubsPriceGroup.objects.all():
        temp_graph = SmkLqdBoardCancelledGraph3()
        temp_graph.price_group = group.name
        temp_graph.count = SmkLqdSubsStandingOrder.objects.filter(status='y',fraud='n',price_group=group.id).count()
        temp_graph.save()


@cronjobs.register
def paypal_updater():
    from common.models import PaypalPost, TemporalPaypalNotification

    for post_obj in PaypalPost.objects.filter(processed=False):
        if(post_obj.paypalpostfield_set.filter(field_name='subscr_id')):
            value = post_obj.paypalpostfield_set.get(field_name='subscr_id')

            if  TemporalPaypalNotification.objects.filter(local_trans_id=post_obj.paypal_custom_id).count() > 0:
                notification = TemporalPaypalNotification.objects.get(local_trans_id=post_obj.paypal_custom_id)

                soi = notification.target_soi

                soi.subs_paypal_id = value.field_value

                soi.save()

                post_obj.processed = True

                notification.proccesed = True

                post_obj.save()

                notification.save()














