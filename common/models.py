from django.db import models
from django.contrib.auth.models import User

# Create your models here.
from backend.models import ResPartner, SmkLqdSubsStandingOrder


class Profile(models.Model):
    user = models.OneToOneField(User)
    customer = models.ForeignKey(ResPartner)


class TemporalPaypalNotification(models.Model):
    notify_date = models.DateTimeField(null=True, blank=True);
    proccesed = models.BooleanField(default=False)
    target_soi = models.ForeignKey(SmkLqdSubsStandingOrder, null= True, blank=True)
    local_trans_id = models.CharField(max_length=100, null=True, blank=True)


class PaypalPost(models.Model):
    paypal_custom_id = models.CharField(max_length=100)
    processed = models.BooleanField(default=False)

class PaypalPostField(models.Model):
    field_name = models.CharField(max_length=50)
    field_value = models.CharField(max_length=150)
    paypal_post = models.ForeignKey(PaypalPost, null=True, blank=True)




