from sld_site import settings
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEImage import MIMEImage
import smtplib
from backend.models import SmkLqdNotificationEmail

def send_email(email_name, to=None, params=None):
    email = SmkLqdNotificationEmail.objects.get(name=email_name)
    subject_value = email.email_subject
    email_content = email.email_content

    from jinja2 import Template

    if params:
        email_content_template = Template(email_content)
        email_content_rendered = email_content_template.render(params)
    else:
        email_content_rendered = email_content

    email_template = Template(email.email_template.template_content)

    html = email_template.render(subject=subject_value,
                                content=email_content_rendered)

    if(email.email_from_addr):
        from_email_addr = email.email_from_addr.email_addr
    else:
        from_email_addr = settings.EMAIL_HOST_USER

    if email.require_addr == 'y':
        to_addr = email.addressee
    else:
        to_addr = to


    msgRoot = MIMEMultipart('related')
    msgRoot['Subject'] = subject_value
    msgRoot['From'] = from_email_addr
    msgRoot['To'] = to_addr
    msgRoot.preamble = subject_value

    msgAlternative = MIMEMultipart('alternative')
    msgRoot.attach(msgAlternative)

    msgText = MIMEText(html, 'html')
    msgAlternative.attach(msgText)

    fp = open(settings.EMAIL_IMAGE, 'rb')
    msgImage = MIMEImage(fp.read())
    fp.close()

    msgImage.add_header('Content-ID', '<image1>')
    msgRoot.attach(msgImage)
    if settings.USE_SSL:
        try:
            smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
            if email.email_server.smtp_user:
                smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
            smt.sendmail(from_email_addr,[to_addr],msgRoot.as_string())
            smt.close()
        except Exception,ex:
            print ex
            pass
    else:
        try:
            smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
            if email.email_server.smtp_user:
                smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
            smt.sendmail(from_email_addr,[to_addr],msgRoot.as_string())
            smt.close()
        except Exception,ex:
            print ex
            pass

