# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'PaypalNotificationFields'
        db.delete_table(u'common_paypalnotificationfields')

        # Adding field 'TemporalPaypalNotification.target_soi'
        db.add_column(u'common_temporalpaypalnotification', 'target_soi',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.SmkLqdSubsStandingOrder'], null=True, blank=True),
                      keep_default=False)

        # Adding field 'TemporalPaypalNotification.local_trans_id'
        db.add_column(u'common_temporalpaypalnotification', 'local_trans_id',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding model 'PaypalNotificationFields'
        db.create_table(u'common_paypalnotificationfields', (
            ('field_value', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('field_name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('notification', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common.TemporalPaypalNotification'])),
        ))
        db.send_create_signal(u'common', ['PaypalNotificationFields'])

        # Deleting field 'TemporalPaypalNotification.target_soi'
        db.delete_column(u'common_temporalpaypalnotification', 'target_soi_id')

        # Deleting field 'TemporalPaypalNotification.local_trans_id'
        db.delete_column(u'common_temporalpaypalnotification', 'local_trans_id')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'backend.productcategory': {
            'Meta': {'object_name': 'ProductCategory', 'db_table': "u'product_category'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'create_uid': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'create_user'", 'null': 'True', 'db_column': "u'create_uid'", 'to': u"orm['backend.ResUsers']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductCategory']", 'null': 'True', 'db_column': "u'parent'", 'blank': 'True'}),
            'parent_left': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'parent_right': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sequence': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'write_uid': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'update_user'", 'null': 'True', 'db_column': "u'write_uid'", 'to': u"orm['backend.ResUsers']"})
        },
        u'backend.productproduct': {
            'Meta': {'object_name': 'ProductProduct', 'db_table': "u'product_product'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'color': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'default_code': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'ean13': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_flavour': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name_template': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'price_extra': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'price_margin': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'product_image': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'product_strength': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductStrength']", 'null': 'True', 'db_column': "u'product_strength'", 'blank': 'True'}),
            'product_tmpl': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductTemplate']", 'db_column': "u'product_tmpl_id'"}),
            'variants': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productstrength': {
            'Meta': {'object_name': 'ProductStrength', 'db_table': "u'product_strength'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.producttemplate': {
            'Meta': {'object_name': 'ProductTemplate', 'db_table': "u'product_template'"},
            'categ': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductCategory']", 'db_column': "u'categ_id'"}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCompany']", 'null': 'True', 'db_column': "u'company_id'", 'blank': 'True'}),
            'cost_method': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description_purchase': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description_sale': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'list_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'loc_case': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'loc_rack': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'loc_row': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'mes_type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'procure_method': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'produce_delay': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'product_manager': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResUsers']", 'null': 'True', 'db_column': "u'product_manager'", 'blank': 'True'}),
            'purchase_ok': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'rental': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sale_delay': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'sale_ok': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'standard_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '65535', 'decimal_places': '65535'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'supply_method': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'uom': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'product_template_uom'", 'db_column': "u'uom_id'", 'to': u"orm['backend.ProductUom']"}),
            'uom_po': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'product_tamplete_uom_po'", 'db_column': "u'uom_po_id'", 'to': u"orm['backend.ProductUom']"}),
            'uos': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'product_template_uos'", 'null': 'True', 'db_column': "u'uos_id'", 'to': u"orm['backend.ProductUom']"}),
            'uos_coeff': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'volume': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'warranty': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'weight_net': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productuom': {
            'Meta': {'object_name': 'ProductUom', 'db_table': "u'product_uom'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductUomCateg']", 'db_column': "u'category'"}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'factor': ('django.db.models.fields.DecimalField', [], {'max_digits': '65535', 'decimal_places': '65535'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'rounding': ('django.db.models.fields.DecimalField', [], {'max_digits': '65535', 'decimal_places': '65535'}),
            'uom_type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productuomcateg': {
            'Meta': {'object_name': 'ProductUomCateg', 'db_table': "u'product_uom_categ'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.rescompany': {
            'Meta': {'object_name': 'ResCompany', 'db_table': "u'res_company'"},
            'account_no': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']", 'null': 'True', 'db_column': "u'account_no'", 'blank': 'True'}),
            'company_registry': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'currency_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128', 'blank': 'True'}),
            'paper_format': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'parent_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'partner_id': ('django.db.models.fields.IntegerField', [], {}),
            'rml_footer1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'rml_header': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'company_rml_header'", 'db_column': "u'rml_header'", 'to': u"orm['backend.ResUsers']"}),
            'rml_header1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'rml_header2': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCurrency']", 'db_column': "u'rml_header2'"}),
            'rml_header3': ('django.db.models.fields.TextField', [], {}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'write_uid': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.rescurrency': {
            'Meta': {'object_name': 'ResCurrency', 'db_table': "u'res_currency'"},
            'accuracy': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'base': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCompany']", 'null': 'True', 'db_column': "u'company'", 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'rounding': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.resdepartment': {
            'Meta': {'object_name': 'ResDepartment', 'db_table': "u'res_department'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.resowner': {
            'Meta': {'object_name': 'ResOwner', 'db_table': "u'res_owner'"},
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.respartner': {
            'Meta': {'object_name': 'ResPartner', 'db_table': "u'res_partner'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'color': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCompany']", 'null': 'True', 'db_column': "u'company_id'", 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'credit_limit': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'customer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'ean13': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            'employee': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResOwner']", 'null': 'True', 'db_column': "u'owner'", 'blank': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']", 'null': 'True', 'db_column': "u'parent_id'", 'blank': 'True'}),
            'password_blank': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'ref': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'supplier': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartnerTitle']", 'null': 'True', 'db_column': "u'title'", 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResUsers']", 'null': 'True', 'db_column': "u'user_id'", 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'vat': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.respartnertitle': {
            'Meta': {'object_name': 'ResPartnerTitle', 'db_table': "u'res_partner_title'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '24'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '46'}),
            'shortcut': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.resusers': {
            'Meta': {'object_name': 'ResUsers', 'db_table': "u'res_users'"},
            'action_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'company_id': ('django.db.models.fields.IntegerField', [], {}),
            'context_lang': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'context_tz': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'department_ids': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResDepartment']", 'null': 'True', 'db_column': "u'department_ids'", 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'login': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64'}),
            'menu_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'menu_tips': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'signature': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'user_email': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.smklqdremoteaddr': {
            'Meta': {'object_name': 'SmkLqdRemoteAddr', 'db_table': "u'smk_lqd_remote_addr'"},
            'addr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'subscriptions_count': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.smklqdsubsdailyrefund': {
            'Meta': {'object_name': 'SmkLqdSubsDailyRefund', 'db_table': "u'smk_lqd_subs_daily_refund'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'refund_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'refunds_due': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'signup_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '1', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'write_uid': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResUsers']", 'null': 'True', 'db_column': "u'write_uid'", 'blank': 'True'})
        },
        u'backend.smklqdsubsflavours': {
            'Meta': {'object_name': 'SmkLqdSubsFlavours', 'db_table': "u'smk_lqd_subs_flavours'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'flavour1': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour1'", 'null': 'True', 'db_column': "u'flavour1'", 'to': u"orm['backend.ProductProduct']"}),
            'flavour10': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour10'", 'null': 'True', 'db_column': "u'flavour10'", 'to': u"orm['backend.ProductProduct']"}),
            'flavour2': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour2'", 'null': 'True', 'db_column': "u'flavour2'", 'to': u"orm['backend.ProductProduct']"}),
            'flavour3': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour3'", 'null': 'True', 'db_column': "u'flavour3'", 'to': u"orm['backend.ProductProduct']"}),
            'flavour4': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour4'", 'null': 'True', 'db_column': "u'flavour4'", 'to': u"orm['backend.ProductProduct']"}),
            'flavour5': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour5'", 'null': 'True', 'db_column': "u'flavour5'", 'to': u"orm['backend.ProductProduct']"}),
            'flavour6': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour6'", 'null': 'True', 'db_column': "u'flavour6'", 'to': u"orm['backend.ProductProduct']"}),
            'flavour7': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour7'", 'null': 'True', 'db_column': "u'flavour7'", 'to': u"orm['backend.ProductProduct']"}),
            'flavour8': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour8'", 'null': 'True', 'db_column': "u'flavour8'", 'to': u"orm['backend.ProductProduct']"}),
            'flavour9': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'flavour_flavour9'", 'null': 'True', 'db_column': "u'flavour9'", 'to': u"orm['backend.ProductProduct']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.smklqdsubsfreeflavours': {
            'Meta': {'object_name': 'SmkLqdSubsFreeFlavours', 'db_table': "u'smk_lqd_subs_free_flavours'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'create_uid': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'free_flavour_create_user'", 'null': 'True', 'db_column': "u'create_uid'", 'to': u"orm['backend.ResUsers']"}),
            'flavour1': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'free_flavour_flavour1'", 'null': 'True', 'db_column': "u'flavour1'", 'to': u"orm['backend.ProductProduct']"}),
            'flavour2': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'free_flavour_flavour2'", 'null': 'True', 'db_column': "u'flavour2'", 'to': u"orm['backend.ProductProduct']"}),
            'flavour3': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'free_flavour_flavour3'", 'null': 'True', 'db_column': "u'flavour3'", 'to': u"orm['backend.ProductProduct']"}),
            'flavour4': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'free_flavour_flavour4'", 'null': 'True', 'db_column': "u'flavour4'", 'to': u"orm['backend.ProductProduct']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'write_uid': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'free_flavour_update_user1'", 'null': 'True', 'db_column': "u'write_uid'", 'to': u"orm['backend.ResUsers']"})
        },
        u'backend.smklqdsubsstandingorder': {
            'Meta': {'object_name': 'SmkLqdSubsStandingOrder', 'db_table': "u'smk_lqd_subs_standing_order'"},
            'battery_counter': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'cancelled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cancelled_on': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'cigarette_smoked_per_day': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'cohort_index': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'created_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']", 'db_column': "u'customer'"}),
            'customer_email': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'customer_name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'customer_zip_code': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'daily_refund_ids': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.SmkLqdSubsDailyRefund']", 'null': 'True', 'db_column': "u'daily_refund_ids'", 'blank': 'True'}),
            'flavours': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.SmkLqdSubsFlavours']", 'null': 'True', 'db_column': "u'flavours'", 'blank': 'True'}),
            'fraud': ('django.db.models.fields.CharField', [], {'default': "u'n'", 'max_length': '5'}),
            'free_flavours': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.SmkLqdSubsFreeFlavours']", 'null': 'True', 'db_column': "u'free_flavours'", 'blank': 'True'}),
            'free_vaporizer_count': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_batchid': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'last_billing': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'last_delivery': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'last_order_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'next_billing': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'next_delivery': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'order_counter': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'potential_fraud': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'remote_addr': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.SmkLqdRemoteAddr']", 'null': 'True', 'db_column': "u'remote_addr'", 'blank': 'True'}),
            'request_cancellation': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'snail_mail_counter': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "u'y'", 'max_length': '1'}),
            'subs_paypal_id': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'successful_billing_count': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'common.profile': {
            'Meta': {'object_name': 'Profile'},
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'common.temporalpaypalnotification': {
            'Meta': {'object_name': 'TemporalPaypalNotification'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'local_trans_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'notify_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'proccesed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'target_soi': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.SmkLqdSubsStandingOrder']", 'null': 'True', 'blank': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['common']