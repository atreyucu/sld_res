/**
 * Created by ernesto on 17/02/14.
 */

function get_towns(town, country){
    var option = "<option selected='selected' value=''>Choose your town/city</option>";
    $.get("/usersadmin/get_towns",
        {"country": country.val()},
        function(data){
            for (value in data){
                var html = "<option value='"+value+"'>"+ data[value] + " </option>";
                option += html;
            }
            town.html(option);
        },
        'JSON')
}

function get_towns_and_counties(county, country){
    var option2 = "<option selected='selected' value=''>Choose your County</option>";

    $.get("/usersadmin/get_counties",
        {"country": country.val()},
        function(data){
            for (value in data){
                var html = "<option value='"+value+"'>"+ data[value] + " </option>";
                option2 += html;
            }
            county.html(option2);
        },
        'JSON')
}

function email_validation(email_value){
    var filter = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
     if(filter.test(email_value.val()))
     {
        console.log('valid email');
        return true;
     }
    else
    {
        email_value.addClass('input_error');
        console.log(email_value.val());
        console.log('invalid email');
        return false;
    }
}

