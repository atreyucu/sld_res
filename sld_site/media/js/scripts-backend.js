$(function(){
    $(".menu-btn").on("click", function(e){
        e.preventDefault();
        $(".menu").fadeToggle();
    });
    var box = $(".box-equalized");
    var maxh=0;
    $.each(box, function() {
        if ($(this).height() > maxh) {
            maxh=$(this).height();
        }
    });
    $.each(box, function() {
        $(this).height(maxh);
    });

    var row = $('.equalize');
    $.each(row, function() {
        var maxh=0;
        $.each($(this).find('div[class^="col-"]'), function() {
            if($(this).height() > maxh)
                maxh=$(this).height();
        });
        $.each($(this).find('div[class^="col-"]'), function() {
            $(this).height(maxh);
        });
    });
});