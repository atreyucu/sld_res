from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'sld_site.views.home', name='home'),
    # url(r'^sld_site/', include('sld_site.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),

    (r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'general_auth/login.html','SSL':True}),
    (r'^accounts/logout/$', 'common.views.logout_view',{'SSL':True}),
    url(r'^', include('frontend.urls')),
    url(r'^usersadmin/', include('frontend.usersadmin_urls')),

    url(r'^captcha/', include('captcha.urls')),
)
