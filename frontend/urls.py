__author__ = 'ernesto'

from django.conf.urls import patterns

urlpatterns = patterns('',
    (r'^$', 'frontend.views.home'),
    # (r'^signup/$', 'frontend.views.sign_up',{'SSL':True}),
    # (r'^signup/$', 'frontend.views.new_signup',{'SSL':True}),
    (r'^started/$', 'frontend.views.new_signup',{'SSL':True}),
    (r'^started2/$', 'frontend.views.new_signup2',{'SSL':True}),
    (r'^started3/$', 'frontend.views.new_signup3',{'SSL':True}),
    (r'^about_us/$', 'frontend.views.about_us'),
    (r'^contact_us/$', 'frontend.views.contact_us'),
    (r'^register_prize/$', 'frontend.views.register_prize',{'SSL':True}),
    (r'^/good_news/$', 'frontend.views.good_news',{'SSL':True}),
    # (r'^signup/middle_step/$', 'frontend.views.middle_step',{'SSL':True}),
    # (r'^signup/input_flavours$', 'frontend.views.sign_up_step2',{'SSL':True}),
    # (r'^signup/input_address$', 'frontend.views.sign_up_step3',{'SSL':True}),
    (r'^password_recover/$', 'frontend.views.forgot_passwd',{'SSL':True}),
    (r'^resend_credentials_by_email/$', 'frontend.views.resend_credentials_by_email',{'SSL':True}),

    #payment url
   (r'^payment_successful/$', 'frontend.views.payment_successful',{'SSL':True}),
   (r'^payment_error/$', 'frontend.views.payment_error',{'SSL':True}),
   (r'^payment_proxy/$', 'frontend.views.payment_proxy',{'SSL':True}),
   (r'^cancel/$', 'frontend.views.cancel_payment',{'SSL':True}),

   (r'^signup/save_delivery_data$', 'frontend.views.get_delivery_data',{'SSL':True}),

   (r'^referral/(?P<invite_id>\d+)/$', 'frontend.views.referral'),




   #------ajax call-------------------------
    (r'^disclaimer_popup/$', 'frontend.views.disclaimer_popup'),
    (r'^terms_popup_for_link/$', 'frontend.views.terms_popup_for_link'),
    (r'^terms_popup/$', 'frontend.views.terms_popup',{'SSL':True}),


    #-----------urls invites----------------
    (r'^socialinvites/invite_proxy/(?P<invite_id>\d+)/$', 'frontend.socialinvites_views.invite_proxy'),
    #(r'^socialinvites/facebook_popup$', 'frontend.socialinvites_views.facebook_popup'),
    (r'^socialinvites/facebook_popup_json$', 'frontend.socialinvites_views.facebook_popup_json'),
    (r'^socialinvites/twitter_popup$', 'frontend.socialinvites_views.twitter_popup',{'SSL':True}),
    (r'^socialinvites/email_popup$', 'frontend.socialinvites_views.email_popup',{'SSL':True}),
    (r'^notification_url/$', 'frontend.views.notification_url'),

)