# Create your views here.
import datetime
from dateutil.relativedelta import relativedelta
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.core.mail.message import EmailMessage
from django.views.decorators.csrf import csrf_exempt
from django.db import transaction
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.template.context import RequestContext
from django.views.decorators.gzip import gzip_page
from django.shortcuts import render_to_response
from backend.models import SmkLqdMessage, ResPartner, ProductProduct, ResPartnerTitle, ResPartnerAddress, SmkLqdSubsStandingOrder, ResCountry, ResCountryState, SmkLqdSubsFlavours, SmkLqdSubsFreeFlavours, SmkLqdRemoteAddr, SmkLqdOrderBatch, SmkLqdOrderOrder, SmkLqdOrderItem, SmkLqdInvites, SmkLqdPrizeRegistration, SmkLqdRecurringPaymentSettings, SmkLqdSubsPriceGroup, LambertCrmCancelledList, LambertCrmPreoffer
from common.models import Profile, TemporalPaypalNotification
from frontend.forms import CaptchaForm
import sld_site.settings as settings
from django.contrib.auth.models import User
from common.date_tools import get_next_billing
from common.fraud import RemoreAddrUtil, AnalyticFraud

from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEImage import MIMEImage


def disclaimer_popup(request):
    return render_to_response('frontend/disclaimer_popup_content.html',{},context_instance=RequestContext(request))

def terms_popup_for_link(request):
    return render_to_response('frontend/terms_popup_content_for_link.html',{},context_instance=RequestContext(request))


def terms_popup(request):
    return render_to_response('frontend/terms_popup_content.html',{},context_instance=RequestContext(request))

@gzip_page
def home(request):
    return render_to_response('frontend/home8.html', {"current": 'index_current', 'is_logged_in': request.user.is_authenticated()}, context_instance = RequestContext(request))

def about_us(request):
    return render_to_response("frontend/about_us.html", {}, context_instance = RequestContext(request))

@gzip_page
def contact_us(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/usersadmin/contactus/')
    if request.POST:
        form = CaptchaForm(request.POST)
        name = request.POST['name']
        email = request.POST['email']
        message_text = request.POST['message']
        if form.is_valid():
            message = SmkLqdMessage()
            message.body = message_text
            message.subject = settings.PUBLIC_SUBJECT_MESSAGE
            message.sender_email = email
            message.sender_name = name
            message.from_site = 'sld'
            message.is_public = True
            message.sent_on = datetime.datetime.now()
            message.type = 'p'
            message.save()

            from common.emails import send_email

            send_email('new_message')

            return render_to_response('frontend/contact_us.html', {'form': form, 'captcha': 1, 'show_delivery_message': True},context_instance = RequestContext(request))
        else:
            return render_to_response('frontend/contact_us.html', {'form': form, 'captcha': 0, 'name':name, 'email':email, 'message':message_text},context_instance = RequestContext(request))
    else:
        form = CaptchaForm()
        return render_to_response('frontend/contact_us.html', {'form': form, 'captcha': -1},context_instance = RequestContext(request))


@gzip_page
def sign_up(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/usersadmin/')

    request.session['comming'] = 'sign_up'
    return render_to_response('frontend/signup_step1.html', {'list': range(1,100)}, context_instance= RequestContext(request))

def middle_step(request):
    if request.method == 'POST':
        if request.POST.has_key('email_address') and  request.POST.has_key('remail_address') and request.POST.has_key('password') and request.POST.has_key('rpassword'):
            email_address = request.POST.get('email_address')
            remail_address = request.POST.get('remail_address')
            password = request.POST.get('password')
            rpassword = request.POST.get('rpassword')
            if password != rpassword or email_address != remail_address:
                return HttpResponseRedirect('/signup')
            if email_address == "":
                return HttpResponseRedirect('/signup')

            request.session['user_data'] = {'email_address': email_address, 'password': password}

            not_exist_user = ResPartner.objects.filter(username=email_address).count() == 0

            if not_exist_user:
                request.session['comming'] = 'good_news'
                return HttpResponseRedirect('/signup/input_flavours?pass=p')

            return HttpResponseRedirect('/signup/good_news')
        else:
            raise Http404
    else:
        raise  Http404


@gzip_page
def good_news(request):
    if request.session.has_key('comming') and request.session['comming'] == 'sign_up':
        request.session['comming'] = 'good_news'

        user_data = request.session['user_data']
        email_address = user_data['email_address']
        user = User.objects.get(username=email_address)

        password = user.profile.customer.password_blank

        from common.emails import send_email
        send_email('password',email_address,{'username': email_address,'password':password})

        return render_to_response('frontend/good_news.html', context_instance= RequestContext(request))
    else:
        return HttpResponseRedirect('/signup/')


@gzip_page
def sign_up_step2(request):
    if request.session.has_key('comming') and request.session['comming'] == 'good_news':
        user_data = request.session['user_data']
        email = user_data['email_address']
        password = user_data['password']

        #Recuperando datos
        all_flavours = ProductProduct.objects.filter(is_flavour = True)
        flavours_user_arr = []
        cigarette_per_day = 0
        flavours_return = []
        count = 0
        for f in all_flavours:
            count +=1
            flavours_temp = {}
            flavours_temp['sld_ext_id'] = f.id
            flavours_temp['size'] = f.product_strength.display_name
            flavours_temp['item_name'] = f.name_template #+ f.product_strength.display_name
            flavours_return.append(flavours_temp)

        flavours_user = {'flavour_1': 0, 'flavour_2': 0,
                        'flavour_3': 0, 'flavour_4': 0,
                        'flavour_5': 0,'flavour_6': 0,
                        'flavour_7': 0,'flavour_8': 0,
                        'flavour_9': 0,'flavour_10': 0,}


        request.session['comming'] = 'input_flavours'
        return render_to_response('frontend/input_flavours.html',{'cigarette_per_day': cigarette_per_day, 'list': range(1,100), 'flavours': flavours_return, 'flavours_user': flavours_user}, context_instance= RequestContext(request))
    else:
        raise Http404



@gzip_page
def sign_up_step3(request):
    if request.session.has_key('comming') and request.session['comming'] == 'input_flavours':
        if request.POST.has_key("flavour_1") and request.POST.has_key("flavour_2") and request.POST.has_key("flavour_3") and request.POST.has_key("flavour_4") and request.POST.has_key("flavour_5") and request.POST.has_key("flavour_6") and request.POST.has_key("flavour_7") and request.POST.has_key("flavour_8") and request.POST.has_key("flavour_9") and request.POST.has_key("flavour_10"):
            #obteniendo los datos del paso  2
            sld_product_id_1 = request.POST["flavour_1"]
            sld_product_id_2 = request.POST["flavour_2"]
            sld_product_id_3 = request.POST["flavour_3"]
            sld_product_id_4 = request.POST["flavour_4"]
            sld_product_id_5 = request.POST["flavour_5"]
            sld_product_id_6 = request.POST["flavour_6"]
            sld_product_id_7 = request.POST["flavour_7"]
            sld_product_id_8 = request.POST["flavour_8"]
            sld_product_id_9 = request.POST["flavour_9"]
            sld_product_id_10 = request.POST["flavour_10"]



            request.session['sld_products'] = {}
            request.session['sld_products']['sld_product_id_1'] = sld_product_id_1
            request.session['sld_products']['sld_product_id_2'] = sld_product_id_2
            request.session['sld_products']['sld_product_id_3'] = sld_product_id_3
            request.session['sld_products']['sld_product_id_4'] = sld_product_id_4
            request.session['sld_products']['sld_product_id_5'] = sld_product_id_5
            request.session['sld_products']['sld_product_id_6'] = sld_product_id_6
            request.session['sld_products']['sld_product_id_7'] = sld_product_id_7
            request.session['sld_products']['sld_product_id_8'] = sld_product_id_8
            request.session['sld_products']['sld_product_id_9'] = sld_product_id_9
            request.session['sld_products']['sld_product_id_10'] = sld_product_id_10

            request.session['cigarrettes_per_day'] = 10


            #obtener datos a devolver
            titles = ResPartnerTitle.objects.filter(domain='smoking')
            temp_titles = {}
            for t in titles:
                temp_titles[t.id] = t.shortcut

            countrys = ResCountry.objects.all()
            temp_country = {}
            for c in countrys:
                temp_country[c.id] = c.name

            county = ResCountryState.objects.all()
            temp_county = {}
            for ct in county:
                temp_county[str(ct.id)] = ct.name

            user_email = request.session['user_data']['email_address']

            user_data = {'title': '', 'first_name': '', 'last_name': '',
                        'hbn': '', 'street_address': '', 'street_2_line': '',
                        'town': '', 'county': 0, 'country': 0, 'postcode': ''}

            request.session['comming'] = 'input_address'
            return render_to_response('frontend/input_address.html', {'user_data': user_data, 'titles': temp_titles, 'country': temp_country, 'county': temp_county}, context_instance= RequestContext(request))
        else:
            raise Http404
    else:
        return HttpResponseRedirect('/signup/')


@gzip_page
def get_delivery_data(request):
    if request.session.has_key('comming') and request.session['comming'] == 'input_address':
        if request.POST.has_key('title') and request.POST.has_key('firstname') and request.POST.has_key('lastname') and request.POST.has_key('hbn') and request.POST.has_key('street_address') and request.POST.has_key('street_address_2line') and request.POST.has_key('towncity') and request.POST.has_key('country') and request.POST.has_key('postcode'):
            #obteniendo datos del input_address
            title = request.POST.get('title')
            firstname = request.POST.get('firstname')
            lastname = request.POST.get('lastname')
            hbn = request.POST.get('hbn')
            street_address= request.POST.get('street_address')
            street_address_2line = request.POST.get('street_address_2line')
            towncity = request.POST.get('towncity')
            country = request.POST.get('country')
            county = request.POST.get('county')
            postcode = request.POST.get('postcode')

            #guardando en la session
            request.session['delivery_data'] = {}
            request.session['delivery_data']['title'] = title
            request.session['delivery_data']['firstname'] = firstname
            request.session['delivery_data']['lastname'] = lastname
            request.session['delivery_data']['hbn'] = hbn
            request.session['delivery_data']['street_address'] = street_address
            request.session['delivery_data']['towncity'] = towncity
            request.session['delivery_data']['street_address_2line'] = street_address_2line
            request.session['delivery_data']['country'] = country
            request.session['delivery_data']['county'] = county
            request.session['delivery_data']['postcode'] = postcode
            request.session['comming'] = 'save_input_address'
            return HttpResponseRedirect('/payment_proxy/')
        else:
            raise Http404
    else:
        return HttpResponseRedirect('/signup/')

@gzip_page
def payment_proxy(request):
    if request.method== 'POST' and request.session.has_key('comming') and request.session['comming'] == 'save_input_address':
        import uuid

        trans_id = str(uuid.uuid4()).replace('-','')
        request.session['waiting_for_payment_reponse'] = 'paypal'
        request.session['paypal_custom_trans_id'] = trans_id

        return render_to_response('frontend/paypal_autosend_form.html',{'notify_url': '', 'custom_trans_id' : trans_id}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/signup/')

@gzip_page
@csrf_exempt
@transaction.commit_manually
def payment_successful(request):
    if(request.session.has_key('waiting_for_payment_reponse')): # and request.META.has_key('HTTP_REFERER') and request.META['HTTP_REFERER'].find('paypal.com') != -1):
        try:
            request.session.__delitem__('waiting_for_payment_reponse')
            title = request.session['delivery_data']['title']
            firstname = request.session['delivery_data']['firstname']
            lastname = request.session['delivery_data']['lastname']
            hbn = request.session['delivery_data']['hbn']
            street_address = request.session['delivery_data']['street_address']
            street_address_2line = request.session['delivery_data']['street_address_2line']
            towncity = request.session['delivery_data']['towncity']
            country = request.session['delivery_data']['country']
            postcode = request.session['delivery_data']['postcode']
            county = request.session['delivery_data']['county']
            user_data = request.session['user_data']

            temp_user_password = user_data['password']

            django_user = User()
            django_user.username = user_data['email_address']
            django_user.first_name = firstname
            django_user.last_name = lastname
            django_user.email = user_data['email_address']
            django_user.is_active = True
            django_user.set_password(user_data['password'])
            django_user.save()

            #customer creation
            customer = ResPartner()
            customer.username = user_data['email_address']
            customer.password_blank = user_data['password']
            customer.title = ResPartnerTitle.objects.get(id=title)
            customer.name = '%s %s'%(firstname,lastname)
            customer.active = True
            customer.customer = True
            customer.supplier = False
            customer.save()


            #customer addrs
            default_addr = ResPartnerAddress()
            default_addr.partner = customer
            default_addr.type = 'default'
            default_addr.title = ResPartnerTitle.objects.get(id=title)
            default_addr.name = firstname
            default_addr.second_name = lastname
            default_addr.hbn = hbn
            default_addr.street = street_address
            default_addr.street2 = street_address_2line
            default_addr.country = ResCountry.objects.get(id=country)
            default_addr.state = ResCountryState.objects.get(id=county)
            default_addr.city = towncity
            default_addr.zip = postcode
            default_addr.email = user_data['email_address']
            default_addr.active = True
            default_addr.save()

            delivery_addr = ResPartnerAddress()
            delivery_addr.partner = customer
            delivery_addr.type = 'delivery'
            delivery_addr.title = ResPartnerTitle.objects.get(id=title)
            delivery_addr.name = firstname
            delivery_addr.second_name = lastname
            delivery_addr.hbn = hbn
            delivery_addr.street = street_address
            delivery_addr.street2 = street_address_2line
            delivery_addr.country = ResCountry.objects.get(id=country)
            delivery_addr.state = ResCountryState.objects.get(id=county)
            delivery_addr.city = towncity
            delivery_addr.zip = postcode
            delivery_addr.email = user_data['email_address']
            delivery_addr.active = True
            delivery_addr.save()

            billing_addr = ResPartnerAddress()
            billing_addr.partner = customer
            billing_addr.type = 'billing'
            billing_addr.title = ResPartnerTitle.objects.get(id=title)
            billing_addr.name = firstname
            billing_addr.second_name = lastname
            billing_addr.hbn = hbn
            billing_addr.street = street_address
            billing_addr.street2 = street_address_2line
            billing_addr.country = ResCountry.objects.get(id=country)
            billing_addr.state = ResCountryState.objects.get(id=county)
            billing_addr.city = towncity
            billing_addr.zip = postcode
            billing_addr.email = user_data['email_address']
            billing_addr.active = True
            billing_addr.save()

            contact_addr = ResPartnerAddress()
            contact_addr.partner = customer
            contact_addr.type = 'contact'
            contact_addr.title = ResPartnerTitle.objects.get(id=title)
            contact_addr.name = firstname
            contact_addr.second_name = lastname
            contact_addr.hbn = hbn
            contact_addr.street = street_address
            contact_addr.street2 = street_address_2line
            contact_addr.country = ResCountry.objects.get(id=country)
            contact_addr.state = ResCountryState.objects.get(id=county)
            contact_addr.city = towncity
            contact_addr.zip = postcode
            contact_addr.email = user_data['email_address']
            contact_addr.active = True
            contact_addr.save()

            user_flavours = SmkLqdSubsFlavours()
            sld_products_session = request.session['sld_products']
            user_flavours.flavour1 = ProductProduct.objects.get(id=sld_products_session['sld_product_id_1'])
            user_flavours.flavour2 = ProductProduct.objects.get(id=sld_products_session['sld_product_id_2'])
            user_flavours.flavour3 = ProductProduct.objects.get(id=sld_products_session['sld_product_id_3'])
            user_flavours.flavour4 = ProductProduct.objects.get(id=sld_products_session['sld_product_id_4'])
            user_flavours.flavour5 = ProductProduct.objects.get(id=sld_products_session['sld_product_id_5'])
            user_flavours.flavour6 = ProductProduct.objects.get(id=sld_products_session['sld_product_id_6'])
            user_flavours.flavour7 = ProductProduct.objects.get(id=sld_products_session['sld_product_id_7'])
            user_flavours.flavour8 = ProductProduct.objects.get(id=sld_products_session['sld_product_id_8'])
            user_flavours.flavour9 = ProductProduct.objects.get(id=sld_products_session['sld_product_id_9'])
            user_flavours.flavour10 = ProductProduct.objects.get(id=sld_products_session['sld_product_id_10'])
            user_flavours.save()

            four_free_flavour = SmkLqdSubsFreeFlavours()
            four_free_flavour.flavour1 = ProductProduct.objects.get(id=sld_products_session['sld_product_id_1'])
            four_free_flavour.flavour2 = ProductProduct.objects.get(id=sld_products_session['sld_product_id_1'])
            four_free_flavour.flavour3 = ProductProduct.objects.get(id=sld_products_session['sld_product_id_1'])
            four_free_flavour.flavour4 = ProductProduct.objects.get(id=sld_products_session['sld_product_id_1'])
            four_free_flavour.save()


            #creando la order en la tabla
            if SmkLqdRecurringPaymentSettings.objects.all().count() > 0:
                recurring_settings = SmkLqdRecurringPaymentSettings.objects.all()[0]

                days_count = recurring_settings.days_per_period
                minus_days = recurring_settings.printing_minus_days
            else:
                days_count = 61
                minus_days = 10


            soi = SmkLqdSubsStandingOrder()
            soi.flavours = user_flavours
            soi.free_flavours = four_free_flavour
            soi.customer = customer
            soi.last_order_date = datetime.date.today()
            soi.last_billing = datetime.date.today()
            soi.last_delivery = datetime.date.today() + datetime.timedelta(days=1)
            soi.last_printed = datetime.date.today()
            soi.next_delivery = datetime.date.today() + datetime.timedelta(days = days_count)
            soi.next_printed = datetime.date.today() + datetime.timedelta(days = days_count - minus_days)
            soi.next_billing = get_next_billing(datetime.date.today())
            soi.create_date = datetime.datetime.now()
            soi.cancelled = False
            soi.free_vaporizer_count = 0
            cohort_time_delta = datetime.date.today() - settings.COHORT_START_DATE
            soi.cohort_index = cohort_time_delta.days/30 + 1
            soi.customer_email = user_data['email_address']
            soi.created_date = datetime.date.today()

            price_group = SmkLqdSubsPriceGroup.objects.get(name__icontains='29.99')

            soi.price_group = price_group


            remote_addr = RemoreAddrUtil().process_request(request)

            if SmkLqdRemoteAddr.objects.filter(addr=remote_addr).count() != 0:
                remote_addr_obj = SmkLqdRemoteAddr.objects.filter(addr=remote_addr)[0]
                soi.remote_addr = remote_addr_obj
                remote_addr_obj.subscriptions_count += 1
                remote_addr_obj.save()
            else:
                remte_addr_obj = SmkLqdRemoteAddr()
                remte_addr_obj.addr = remote_addr
                remte_addr_obj.save()
                soi.remote_addr = remte_addr_obj

            soi.save()

            paypal_notify_object = TemporalPaypalNotification()
            paypal_notify_object.notify_date = datetime.datetime.now()
            paypal_notify_object.proccesed = False;
            paypal_notify_object.target_soi = soi
            paypal_notify_object.local_trans_id = request.session['paypal_custom_trans_id']
            paypal_notify_object.save()


            analytic = AnalyticFraud()
            fraud_result = analytic.analice_fraud(soi)
            soi.potential_fraud =  fraud_result
            if fraud_result:
                soi.fraud = 'y'
            else:
                soi.fraud = 'n'

            soi.customer_name = firstname + ' ' + lastname
            soi.customer_zip_code = postcode


            soi.save()

            # OJO aqui generar una orden para esta trasaccion
            order = SmkLqdOrderOrder()
            order.create_date = datetime.datetime.now()
            order.created_on = datetime.date.today()
            order.customer = customer
            order.customer_billing_hbn = hbn
            order.customer_billing_street = street_address
            order.customer_billing_street_line2 = street_address_2line
            order.customer_billing_country = ResCountry.objects.get(id=country).name
            order.customer_billing_county = ResCountryState.objects.get(id=county).name
            order.customer_billing_city = towncity
            order.customer_billing_postcode = postcode

            order.customer_delivery_hbn = hbn
            order.customer_delivery_street = street_address
            order.customer_delivery_street_line2 = street_address_2line
            order.customer_delivery_country = ResCountry.objects.get(id=country).name
            order.customer_delivery_county = ResCountryState.objects.get(id=county).name
            order.customer_delivery_city = towncity
            order.customer_delivery_postcode = postcode

            if(fraud_result):
                order.potential_fraud = 'y'
            else:
                order.potential_fraud = 'n'
            order.require_envelope = 'y'
            order.envelope_printed = 'n'
            order.printed = 'n'
            order.manual = 'n'
            order.customer_name = '%s %s'%(firstname,lastname)
            import random
            import time

            order.trans_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
            order.customer_email = user_data['email_address']
            order.customer_fax = ''
            order.customer_mobile = ''
            order.customer_phone = ''
            order.order_total = 19.99

            if fraud_result:
                if SmkLqdOrderBatch.objects.filter(potential_fraud='y',require_envelope='y').count() > 0:
                    if SmkLqdOrderBatch.objects.filter(potential_fraud='y',require_envelope='y').order_by('-id')[0].smklqdorderorder_set.count() <= settings.DEFAULT_ORDER_NUMBER_PER_BATCH and not SmkLqdOrderBatch.objects.filter(potential_fraud='y',require_envelope='y').order_by('-id')[0].printed == 'y':
                        batch = SmkLqdOrderBatch.objects.filter(potential_fraud='y',require_envelope='y').order_by('-id')[0]
                        batch.order_count += 1
                        batch.save()
                    else:
                        batch = SmkLqdOrderBatch()
                        batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
                        batch.created_on = datetime.date.today()
                        batch.potential_fraud = 'y'
                        batch.require_envelope = 'y'
                        batch.manual = 'n'
                        batch.order_count = 1
                        batch.envelope_printed = 'n'
                        batch.printed = 'n'
                        batch.save()
                else:
                    batch = SmkLqdOrderBatch()
                    batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
                    batch.created_on = datetime.date.today()
                    batch.potential_fraud = 'y'
                    batch.require_envelope = 'y'
                    batch.manual = 'n'
                    batch.envelope_printed = 'n'
                    batch.order_count = 1
                    batch.printed = 'n'
                    batch.save()
            else:
                if SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='y').count() > 0:
                    if SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='y').order_by('-id')[0].smklqdorderorder_set.count() <= settings.DEFAULT_ORDER_NUMBER_PER_BATCH and not SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='y').order_by('-id')[0].printed == 'y':
                        batch = SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='y').order_by('-id')[0]
                        batch.order_count += 1
                        batch.save()
                    else:
                        batch = SmkLqdOrderBatch()
                        batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
                        batch.created_on = datetime.date.today()
                        batch.potential_fraud = 'n'
                        batch.require_envelope = 'y'
                        batch.manual = 'n'
                        batch.order_count = 1
                        batch.envelope_printed = 'n'
                        batch.printed = 'n'
                        batch.save()
                else:
                    batch = SmkLqdOrderBatch()
                    batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
                    batch.created_on = datetime.date.today()
                    batch.potential_fraud = 'n'
                    batch.require_envelope = 'y'
                    batch.manual = 'n'
                    batch.order_count = 1
                    batch.envelope_printed = 'n'
                    batch.printed = 'n'
                    batch.save()

            order.batch_ids = batch
            order.save()

            #item para el flavour 1
            temp_product = ProductProduct.objects.get(id=sld_products_session['sld_product_id_1'])
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2
            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 2
            temp_product = ProductProduct.objects.get(id=sld_products_session['sld_product_id_2'])
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2
            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 3
            temp_product = ProductProduct.objects.get(id=sld_products_session['sld_product_id_3'])
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2
            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 4
            temp_product = ProductProduct.objects.get(id=sld_products_session['sld_product_id_4'])
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 5
            temp_product = ProductProduct.objects.get(id=sld_products_session['sld_product_id_5'])
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 6
            temp_product = ProductProduct.objects.get(id=sld_products_session['sld_product_id_6'])
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 7
            temp_product = ProductProduct.objects.get(id=sld_products_session['sld_product_id_7'])
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 8
            temp_product = ProductProduct.objects.get(id=sld_products_session['sld_product_id_8'])
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 9
            temp_product = ProductProduct.objects.get(id=sld_products_session['sld_product_id_9'])
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 10
            temp_product = ProductProduct.objects.get(id=sld_products_session['sld_product_id_10'])
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #addning the smoking pipe
            temp_product = ProductProduct.objects.get(default_code='C-1')
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 1

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()


            #addning the usb plug
            temp_product = ProductProduct.objects.get(default_code='D-1')
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 1

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #addning the free vaporizeer
            temp_product = ProductProduct.objects.get(default_code='F-1')
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            soi.battery_counter = 2
            soi.last_batchid = batch.batch_id
            soi.order_counter = 2
            soi.save()

            redirect_to_referral = False

            profile = Profile()
            profile.customer = customer
            profile.user = django_user
            profile.save()


            if request.session.has_key('sld_customer_follow_invite'):
                inv_id = request.session['sld_customer_follow_invite']
                invite = SmkLqdInvites.objects.get(id=inv_id)
                invite.convert_signup = True
                invite.save()
                customer = invite.sender
                sender_soi = customer.smklqdsubsstandingorder_set.all()[0]
                sender_soi.free_vaporizer_count += 1
                sender_soi.save()
                request.session.__delitem__('sld_customer_follow_invite')
                redirect_to_referral = True

            request.session.__delitem__('paypal_custom_trans_id')


            if(SmkLqdPrizeRegistration.objects.filter(email=user_data['email_address']).count() != 0):
                SmkLqdPrizeRegistration.objects.filter(email=user_data['email_address']).delete()


            from django.contrib.auth import login, authenticate
            authenticated_user = authenticate(username = user_data['email_address'], password = user_data['password'])
            login(request,authenticated_user)

            from common.emails import send_email

            send_email('account_details',request.user.email,{'password':temp_user_password})
            send_email('bonus3',request.user.email)
        except Exception, e:
            transaction.rollback()
            return HttpResponse(e)
            return render_to_response('500.html', {}, context_instance = RequestContext(request))

        if redirect_to_referral:
            invite = SmkLqdInvites.objects.get(id=inv_id)

            send_email('gift',invite.sender.username)

            transaction.commit()

            return render_to_response('frontend/payment_successful.html',context_instance = RequestContext(request))
            #return HttpResponseRedirect('/referral/' + str(inv_id) + '/')
        else:
            transaction.commit()
            return render_to_response('frontend/payment_successful.html',context_instance = RequestContext(request))

    elif request.session.has_key('paypal_activate_account'): #and request.META.has_key('HTTP_REFERER') and request.META['HTTP_REFERER'].find('paypal.com') != -1:
        try:
            soi = SmkLqdSubsStandingOrder.objects.get(id=request.session['paypal_activate_account'])
            request.session.__delitem__('paypal_activate_account')
            soi.cancelled = False
            soi.status = 'y'
            soi.reactivated = 'y'
            soi.reactivated_date = datetime.date.today()
            soi.save()

            paypal_notify_object = TemporalPaypalNotification()
            paypal_notify_object.notify_date = datetime.datetime.now()
            paypal_notify_object.proccesed = False;
            paypal_notify_object.target_soi = soi
            paypal_notify_object.local_trans_id = request.session['paypal_custom_trans_id']
            paypal_notify_object.save()


            user_flavours = soi.flavours
            # OJO aqui generar una orden para esta trasaccion
            # OJO aqui generar una orden para esta trasaccion
            order = SmkLqdOrderOrder()
            order.create_date = datetime.datetime.now()
            order.created_on = datetime.date.today()
            order.customer = soi.customer
            billing_addr = soi.customer.respartneraddress_set.filter(type='billing')[0]
            delivery_addr = soi.customer.respartneraddress_set.filter(type='delivery')[0]
            order.customer_billing_hbn = billing_addr.hbn
            order.customer_billing_street = billing_addr.street
            order.customer_billing_street_line2 = billing_addr.street2
            order.customer_billing_country = billing_addr.country.name
            order.customer_billing_county = billing_addr.state.name
            order.customer_billing_city = billing_addr.city
            order.customer_billing_postcode = billing_addr.zip

            order.customer_delivery_hbn = delivery_addr.hbn
            order.customer_delivery_street = delivery_addr.street
            order.customer_delivery_street_line2 = delivery_addr.street2
            order.customer_delivery_country = delivery_addr.country.name
            order.customer_delivery_county = delivery_addr.state.name
            order.customer_delivery_city = delivery_addr.city
            order.customer_delivery_postcode = delivery_addr.zip

            order.require_envelope = 'n'
            order.printed = 'n'
            order.manual = 'n'
            order.customer_name = soi.customer.name
            import random
            import time
            order.trans_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
            order.customer_email = soi.customer.username
            order.customer_fax = delivery_addr.fax
            order.customer_mobile = delivery_addr.mobile
            order.customer_phone = delivery_addr.phone

            if SmkLqdOrderBatch.objects.filter(require_envelope='n').count() > 0:
                    if SmkLqdOrderBatch.objects.filter(require_envelope='n').order_by('-id')[0].smklqdorderorder_set.count() <= settings.DEFAULT_ORDER_NUMBER_PER_BATCH and not SmkLqdOrderBatch.objects.filter(require_envelope='n').order_by('-id')[0].printed == 'y':
                        batch = SmkLqdOrderBatch.objects.filter(require_envelope='n').order_by('-id')[0]
                        batch.order_count += 1
                        batch.save()
                    else:
                        batch = SmkLqdOrderBatch()
                        batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
                        batch.created_on = datetime.date.today()
                        batch.potential_fraud = 'n'
                        batch.require_envelope = 'n'
                        batch.manual = 'n'
                        batch.order_count = 1
                        batch.envelope_printed = 'n'
                        batch.printed = 'n'
                        batch.save()
            else:
                batch = SmkLqdOrderBatch()
                batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
                batch.created_on = datetime.date.today()
                batch.potential_fraud = 'n'
                batch.require_envelope = 'n'
                batch.manual = 'n'
                batch.order_count = 1
                batch.envelope_printed = 'n'
                batch.printed = 'n'
                batch.save()


            order.batch_ids = batch
            order.save()

            #item para el flavour 1
            temp_product = soi.flavours.flavour1
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 2
            temp_product = soi.flavours.flavour2
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 3
            temp_product = soi.flavours.flavour3
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 4
            temp_product = soi.flavours.flavour4
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 5
            temp_product = soi.flavours.flavour5
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 6
            temp_product = soi.flavours.flavour6
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 7
            temp_product = soi.flavours.flavour7
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 8
            temp_product = soi.flavours.flavour8
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 9
            temp_product = soi.flavours.flavour9
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()

            #item para el flavour 10
            temp_product = soi.flavours.flavour10
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()


            temp_product = ProductProduct.objects.get(default_code='F-1')
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = 2

            item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()



            soi.last_batchid = batch.batch_id
            soi.order_counter += 1
            soi.save()

            recurring_settings = SmkLqdRecurringPaymentSettings.objects.all()[0]

            days_count = recurring_settings.days_per_period
            minus_days = recurring_settings.printing_minus_days

            soi.last_order_date = datetime.date.today()
            soi.free_vaporizer_count = 0
            soi.last_billing = datetime.date.today()
            soi.last_delivery = datetime.date.today() + datetime.timedelta(days=1)
            soi.last_printed = datetime.date.today()
            soi.next_delivery = datetime.date.today() + datetime.timedelta(days = days_count)
            soi.next_printed = datetime.date.today() + datetime.timedelta(days = days_count - minus_days)
            soi.next_billing = get_next_billing(datetime.date.today())
            soi.save()

            request.session.__delitem__('paypal_custom_trans_id')

            from common.emails import send_email

            send_email('reactivation_email',soi.customer_email)

            transaction.commit()
            return HttpResponseRedirect('/usersadmin/')
        except Exception, e:
            transaction.rollback()
            return HttpResponse(e)
            return render_to_response('500.html', {}, context_instance = RequestContext(request))

    else:
        transaction.rollback()
        return HttpResponseRedirect('/')

@gzip_page
def payment_error(request):
    return render_to_response('frontend/payment_error.html',context_instance = RequestContext(request))

@gzip_page
def forgot_passwd(request):
    try:
        if request.POST:
            email_address = request.POST['email']
            customer = ResPartner.objects.get(username = email_address)
            #from sld_site.automated_emails.password_request import subject
            #from sld_site.automated_emails.password_request import content

            #customer = ResPartner.objects.get(username = email_address)
            #content = content%(email_address,customer.password_blank)
            #if settings.USE_SSL:
            #    import smtplib
            #    try:
            #        smtp = smtplib.SMTP_SSL(host=settings.EMAIL_HOST,port=settings.EMAIL_PORT)
            #        message = EmailMessage(subject=subject,body=content, from_email=settings.EMAIL_HOST_USER, to=[email_address])
            #        smtp.sendmail(settings.EMAIL_HOST_USER,[email_address],message.message().as_string())
            #        smtp.close()
            #    except Exception,e:
            #        return HttpResponseRedirect('/')
            #else:
            #    try:
            #        send_mail(subject, content, settings.SLD_FROM_USER, [email_address])
            #    except Exception:
            #        return HttpResponseRedirect('/')
            from common.emails import send_email

            try:
                send_email('password',to=email_address,params={'username':email_address,'password':customer.password_blank})
            except Exception:
                return HttpResponseRedirect('/')

            #return HttpResponseRedirect('/accounts/login/?next=/usersadmin/')
            return render_to_response("frontend/login_recover.html", context_instance=RequestContext(request))

        else:
            return render_to_response("frontend/password_recover.html", {"sucefull":False}, context_instance=RequestContext(request))

    except ObjectDoesNotExist, e:
        return render_to_response("frontend/password_recover.html", {"sucefull":False}, context_instance=RequestContext(request))
    except Exception, e:
        return render_to_response("frontend/password_recover.html", {"sucefull":False}, context_instance=RequestContext(request))


def cancel_payment(request):
    if(request.session.has_key('waiting_for_payment_reponse')): #and request.META.has_key('HTTP_REFERER') and request.META['HTTP_REFERER'].find('paypal.com') != -1):
        try:
            request.session.__delitem__('waiting_for_payment_reponse')
            request.session.__delitem__('user_data')
            request.session.__delitem__('delivery_data')
            request.session.__delitem__('sld_products')
            return HttpResponseRedirect('/')
        except Exception, ex:
            return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')

def referral(request,invite_id):
    from common.emails import send_email
    invite = SmkLqdInvites.objects.get(id=invite_id)

    send_email('gift',invite.sender.username)

    return render_to_response('frontend/payment_successful.html',context_instance = RequestContext(request))


def resend_credentials_by_email(request):
    from sld_site.automated_emails.password_request import subject
    from sld_site.automated_emails.password_request import content

    request.session['comming'] = 'sign_up'

    user_data = request.session['user_data']
    email_address = user_data['email_address']
    customer = ResPartner.objects.get(username=email_address)
    content = content%(user_data['email_address'], customer.password_blank)
    if settings.USE_SSL:
        import smtplib
        try:
            smtp = smtplib.SMTP_SSL(host=settings.EMAIL_HOST,port=settings.EMAIL_PORT)
            message = EmailMessage(subject=subject,body=content, from_email=settings.EMAIL_HOST_USER, to=[user_data['email_address']])
            smtp.sendmail(settings.EMAIL_HOST_USER,[user_data['email_address']],message.message().as_string())
            smtp.close()
        except Exception, e:
            return HttpResponseRedirect('/accounts/login/?next=/usersadmin/')
    else:
        try:
            send_mail(subject, content, settings.SLD_FROM_USER, [user_data['email_address']])
        except Exception:
            return HttpResponseRedirect('/accounts/login/?next=/usersadmin/')


    return HttpResponseRedirect('/signup/good_news/')


@csrf_exempt
def notification_url(request):
    from jinja2 import Template
    from backend.models import SmkLqdNotificationEmail, IrMailServer
    import smtplib
    try:
        if(request.POST.has_key('txn_type') and request.POST.get('txn_type') == 'subscr_signup'):
            from common.models import PaypalPost, PaypalPostField

            paypal_post = PaypalPost()
            paypal_post.paypal_custom_id = request.POST.get('custom')
            paypal_post.processed = False
            paypal_post.save()

            post_field = PaypalPostField()
            post_field.field_name = 'subscr_id'
            post_field.field_value = request.POST.get('subscr_id')
            post_field.paypal_post = paypal_post;
            post_field.save()

        elif(request.POST.has_key('txn_type') and request.POST.get('txn_type') == 'subscr_cancel'):
            soi = SmkLqdSubsStandingOrder.objects.filter(subs_paypal_id=request.POST.get('subscr_id'))
            if(soi):
                last_order = SmkLqdOrderOrder.objects.all().order_by('-id')[0]

                if(last_order.printed == 'y' and (last_order.printed_on - datetime.date.today()).days == 0):
                    can_list = LambertCrmCancelledList()
                    can_list.customer = soi
                    can_list.last_delivery = soi.last_delivery
                    can_list.next_delivery = soi.next_delivery
                    can_list.name = soi.customer_name
                    can_list.email = soi.customer_email
                    can_list.status = 'pend'
                    can_list.save()


                    soi = SmkLqdSubsStandingOrder.objects.get(subs_paypal_id=request.POST.get('subscr_id'))
                    soi.cancelled = True
                    soi.auto_cancelled = 'y'
                    soi.status = 'n'
                    soi.cancelled_on = datetime.date.today()
                    soi.save()

                    try:
                        email_obj = SmkLqdNotificationEmail.objects.get(name='cancel_order')

                        template_content = email_obj.email_template.template_content

                        email_template = Template(template_content)

                        render_content = email_template.render(content=email_obj.email_content)

                        html = str(render_content)

                        from_email_addr = email_obj.email_from_addr.email_addr

                        to_addr = soi.customer_email

                        msgRoot = MIMEMultipart('related')
                        msgRoot['Subject'] = email_obj.email_subject
                        msgRoot['From'] = from_email_addr
                        msgRoot['To'] = to_addr
                        msgRoot.preamble = email_obj.email_subject

                        msgAlternative = MIMEMultipart('alternative')
                        msgRoot.attach(msgAlternative)

                        msgText = MIMEText(html, 'html')
                        msgAlternative.attach(msgText)

                        email_server = IrMailServer.objects.get(name='Mandril')

                        if email_server.smtp_encryption != 'none':
                            try:
                                smt = smtplib.SMTP_SSL(host=email_server.smtp_host,port=email_server.smtp_port)
                                if email_server.smtp_user:
                                    smt.login(email_server.smtp_user,email_server.smtp_pass)
                                smt.sendmail(from_email_addr,[to_addr],msgRoot.as_string())
                                smt.close()
                            except Exception,ex:
                                pass
                        else:
                            try:
                                smt = smtplib.SMTP(host=email_server.smtp_host,port=email_server.smtp_port)
                                if email_server.smtp_user:
                                    smt.login(email_server.smtp_user,email_server.smtp_pass)
                                smt.sendmail(from_email_addr,[to_addr],msgRoot.as_string())
                                smt.close()
                            except Exception,ex:
                                pass
                    except Exception,ex:
                        pass

                    #
                    # for order in soi.customer.smklqdorderorder_set.filter(printed='n'):
                    #     for item in order.smklqdorderitem_set.all():
                    #         item.delete()
                    #     order.delete()
                    #
                    #     if(order.batch_ids.smklqdorderorder_set.count() == 0):
                    #         temp_batch = order.batch_ids
                    #         temp_batch.delete()
                    #     else:
                    #         temp_batch = order.batch_ids
                    #         temp_batch.order_count = temp_batch.smklqdorderorder_set.count()
                    #         temp_batch.save()
                    #
                elif(last_order.printed == 'n' and (last_order.printed_on - datetime.date.today()).days == 0):
                    preoffer = LambertCrmPreoffer()
                    preoffer.customer = soi.customer
                    preoffer.name = soi.customer_name
                    preoffer.email = soi.customer_email
                    preoffer.notes = 'This subscription was cancelled by the user using Paypal.'
                    preoffer.pending = 'y'
                    preoffer.stage = '24.99'
                    preoffer.type = 'pre'
                    preoffer.save()

                    import random
                    import time

                    if SmkLqdOrderBatch.objects.filter(crm='y').count() > 0:
                        if SmkLqdOrderBatch.objects.filter(crm='y').order_by('-id')[0].smklqdorderorder_set.count() <= settings.DEFAULT_ORDER_NUMBER_PER_BATCH and not SmkLqdOrderBatch.objects.filter(crm='y').order_by('-id')[0].printed == 'y':
                            batch = SmkLqdOrderBatch.objects.filter(crm='y').order_by('-id')[0]
                            batch.order_count += 1
                            batch.save()
                        else:
                            batch = SmkLqdOrderBatch()
                            batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
                            batch.created_on = datetime.date.today()
                            batch.potential_fraud = 'n'
                            batch.require_envelope = 'y'
                            batch.manual = 'n'
                            batch.crm = 'y'
                            batch.order_count = 1
                            batch.envelope_printed = 'n'
                            batch.printed = 'n'
                            batch.save()
                    else:
                        batch = SmkLqdOrderBatch()
                        batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
                        batch.created_on = datetime.date.today()
                        batch.potential_fraud = 'n'
                        batch.require_envelope = 'y'
                        batch.manual = 'n'
                        batch.crm = 'y'
                        batch.order_count = 1
                        batch.envelope_printed = 'n'
                        batch.printed = 'n'
                        batch.save()

                        temp_batch = last_order.batch_ids

                        last_order.batch_ids = batch
                        last_order.save()

                        if(temp_batch.smklqdorderorder_set.count() == 1):
                            temp_batch = last_order.batch_ids
                            temp_batch.delete()
                        else:
                            temp_batch = last_order.batch_ids
                            temp_batch.order_count = temp_batch.smklqdorderorder_set.count() - 1
                            temp_batch.save()
                else:
                    preoffer = LambertCrmPreoffer()
                    preoffer.customer = soi.customer
                    preoffer.name = soi.customer_name
                    preoffer.email = soi.customer_email
                    preoffer.notes = 'This subscription was cancelled by the user using Paypal.'
                    preoffer.pending = 'y'
                    preoffer.stage = '24.99'
                    preoffer.type = 'pre'
                    preoffer.save()

        elif(request.POST.has_key('txn_type') and request.POST.get('txn_type') == 'subscr_failed'):
            soi = SmkLqdSubsStandingOrder.objects.filter(subs_paypal_id=request.POST.get('subscr_id'))
            from common.emails import send_email

            if(soi):
                soi = SmkLqdSubsStandingOrder.objects.get(subs_paypal_id=request.POST.get('subscr_id'))
                send_email('ipn_failed',to=soi.customer_email)
        else:
            soi = SmkLqdSubsStandingOrder.objects.filter(subs_paypal_id=request.POST.get('subscr_id'))
            if(soi):
                soi_obj = SmkLqdSubsStandingOrder.objects.get(subs_paypal_id=request.POST.get('subscr_id'))
                soi_obj.subs_paypal_id = request.POST.get('subscr_id')
                soi_obj.save()
        #
        #logfile = open(os.path.join(settings.MEDIA_ROOT, 'temp/logfile'),'a')
        #
        #temp_date = str(datetime.datetime.now())
        #
        #logfile.write(temp_date + '\n')
        #
        #logfile.write('##################################Begin %s \n###########################'%(temp_date))
        #
        #logfile.write('##################################Request\n###########################')
        #
        #logfile.write(str(request) + '\n')
        #
        #logfile.write('##################################End %s\n###########################'%(temp_date))


        return HttpResponse(status=200)
    except Exception, ex:
        #logfile = open(os.path.join(settings.MEDIA_ROOT, 'temp/logfile'),'a')
        #
        #temp_date = str(datetime.datetime.now())
        #
        #logfile.write(temp_date + '\n')
        #
        #
        #logfile.write('##################################Begin %s \n###########################'%(temp_date))
        #
        #logfile.write('##################################Exception\n###########################')
        #
        #logfile.write(str(ex) + '\n')
        #
        #logfile.write('##################################Request\n###########################')
        #
        #logfile.write(str(request) + '\n')
        #
        #logfile.write('##################################End %s\n###########################'%(temp_date))

        return HttpResponse(status=200)

def register_prize(request):
    if request.is_ajax() and request.method == 'POST':
        if request.POST['email']:
            from backend.models import SmkLqdPrizeRegistration
            import json

            exist_prize = SmkLqdPrizeRegistration.objects.filter(email=request.POST['email']).count()

            exist_user = User.objects.filter(email=request.POST['email']).count()

            if exist_prize == 0 and exist_user == 0:
                reg = SmkLqdPrizeRegistration()
                reg.email =  request.POST['email']
                reg.username = request.POST['email']
                reg.first_name = ''
                reg.last_name = ''
                reg.signup_date = datetime.date.today()
                reg.save()

            data = {'success': True, 'text': ''}

            return HttpResponse(json.dumps(data), mimetype='application/json')
        else:
            data = {'success': True, 'text': ''}

            return HttpResponse(json.dumps(data), mimetype='application/json')


    return HttpResponse()


def new_signup(request):
    all_flavours = ProductProduct.objects.filter(is_flavour = True)
    flavours_return = []
    for f in all_flavours:
        flavours_temp = {}
        flavours_temp['sld_ext_id'] = f.id
        flavours_temp['size'] = f.product_strength.display_name
        flavours_temp['item_name'] = f.name_template
        flavours_return.append(flavours_temp)


    titles = ResPartnerTitle.objects.filter(domain='smoking')
    temp_titles = {}
    for t in titles:
        temp_titles[t.id] = t.shortcut

    countrys = ResCountry.objects.all()
    temp_country = {}
    for c in countrys:
        temp_country[c.id] = c.name

    county = ResCountryState.objects.all()
    temp_county = {}
    for ct in county:
        temp_county[str(ct.id)] = ct.name


    request.session['comming'] = 'input_flavours'

    return render_to_response('frontend/signup_new.html', {'flavours': flavours_return, 'titles': temp_titles, 'countries': temp_country, 'counties': temp_county}, context_instance= RequestContext(request))


def new_signup2(request):
    if request.method == 'POST':
        if request.POST.has_key('email') and request.POST.has_key('password'):
            email_address = request.POST.get('email')
            password = request.POST.get('password')

            request.session['user_data'] = {'email_address': email_address, 'password': password}

            not_exist_user = ResPartner.objects.filter(username=email_address).count() == 0

            if not_exist_user:
                sld_product_id_1 = request.POST["flavour_1"]
                sld_product_id_2 = request.POST["flavour_2"]
                sld_product_id_3 = request.POST["flavour_3"]
                sld_product_id_4 = request.POST["flavour_4"]
                sld_product_id_5 = request.POST["flavour_5"]
                sld_product_id_6 = request.POST["flavour_6"]
                sld_product_id_7 = request.POST["flavour_7"]
                sld_product_id_8 = request.POST["flavour_8"]
                sld_product_id_9 = request.POST["flavour_9"]
                sld_product_id_10 = request.POST["flavour_10"]



                request.session['sld_products'] = {}
                request.session['sld_products']['sld_product_id_1'] = sld_product_id_1
                request.session['sld_products']['sld_product_id_2'] = sld_product_id_2
                request.session['sld_products']['sld_product_id_3'] = sld_product_id_3
                request.session['sld_products']['sld_product_id_4'] = sld_product_id_4
                request.session['sld_products']['sld_product_id_5'] = sld_product_id_5
                request.session['sld_products']['sld_product_id_6'] = sld_product_id_6
                request.session['sld_products']['sld_product_id_7'] = sld_product_id_7
                request.session['sld_products']['sld_product_id_8'] = sld_product_id_8
                request.session['sld_products']['sld_product_id_9'] = sld_product_id_9
                request.session['sld_products']['sld_product_id_10'] = sld_product_id_10

                request.session['cigarrettes_per_day'] = 10


                title = request.POST.get('title')
                firstname = request.POST.get('first_name')
                lastname = request.POST.get('last_name')
                hbn = request.POST.get('hbn')
                street_address= request.POST.get('addr_ln1')
                street_address_2line = request.POST.get('addr_ln2')
                towncity = request.POST.get('town')
                country = request.POST.get('country')
                county = request.POST.get('county')
                postcode = request.POST.get('postcode')

                #guardando en la session
                request.session['delivery_data'] = {}
                request.session['delivery_data']['title'] = title
                request.session['delivery_data']['firstname'] = firstname
                request.session['delivery_data']['lastname'] = lastname
                request.session['delivery_data']['hbn'] = hbn
                request.session['delivery_data']['street_address'] = street_address
                request.session['delivery_data']['towncity'] = towncity
                request.session['delivery_data']['street_address_2line'] = street_address_2line
                request.session['delivery_data']['country'] = country
                request.session['delivery_data']['county'] = county
                request.session['delivery_data']['postcode'] = postcode

                request.session['comming'] = 'save_input_address'

                return render_to_response('frontend/signup_new2.html', {'list': range(1,100)}, context_instance= RequestContext(request))

            return HttpResponseRedirect('/good_news/')
        else:
            raise Http404
    else:
        raise  Http404



def new_signup3(request):
    return render_to_response('frontend/signup_new3.html', {'list': range(1,100)}, context_instance= RequestContext(request))

